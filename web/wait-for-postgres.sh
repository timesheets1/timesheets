#!/bin/bash
set -e

python manage.py migrate
python manage.py collectstatic --noinput

echo "Running app with gunicorn"
gunicorn --bind=0.0.0.0 -w 4 webTimesheets.wsgi
