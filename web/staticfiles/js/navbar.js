function toggleNavbar() {
    var navbarLinks = document.getElementById("navbarLinks");
    if (navbarLinks.classList.contains("hidden")) {
        navbarLinks.classList.remove("hidden");
    } else {
        navbarLinks.classList.add("hidden");
    }
}

function hideNavbar() {
    var navbarLinks = document.getElementById("navbarLinks");
    if (navbarLinks) {
        if (navbarLinks.classList.contains("hidden")) {
        } else {
            navbarLinks.classList.add("hidden");
        }
    }

}

function hideNavbarAtBreakpoint() {
    var w = window.innerWidth;
    if (w > 768) { // tailwinds md: breakpoint
        hideNavbar();
    }
}