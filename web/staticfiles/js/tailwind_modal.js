current_modal_id = null;

function toggleModal(modalId) {
    var modal = document.getElementById(modalId);
    current_modal_id = modalId;

    if (modal.classList.contains("hidden")) {
        openModal(modalId);
    } else {
        closeModal(modalId);
    }
}


function closeModal(modalId) {
    var modal = document.getElementById(modalId);

    modal.classList.add("hidden");
    current_modal_id = null;
}

function openModal(modalId) {
    var modal = document.getElementById(modalId);
    modal.classList.remove("hidden");
}


// close modal on escape
document.body.onkeydown = function (event) {
    if (event.key === "Escape") {
        var modal = document.getElementById(current_modal_id);

        if (!modal.classList.contains("hidden") && current_modal_id) {
            closeModal(current_modal_id);
        }
    }
}
