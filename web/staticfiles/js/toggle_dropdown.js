function toggleDropdown(dropdown_id){
    var dropdown = document.getElementById(dropdown_id);
    if (dropdown.classList.contains("hidden")){
       dropdown.classList.remove("hidden");
    } else {
       dropdown.classList.add("hidden");
    }
}