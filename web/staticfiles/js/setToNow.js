function setToNowTime(formFieldId) {
    var now = new Date();

    dateString = now.getHours().toString().padStart(2, "0") + ":" + now.getMinutes().toString().padStart(2, "0") + ":" + now.getSeconds().toString().padStart(2, "0");

    formField = document.getElementById(formFieldId);
    formField.value = dateString;
}

function setToNowTimeHM(formFieldId) {
    var now = new Date();

    dateString = now.getHours().toString().padStart(2, "0") + ":" + now.getMinutes().toString().padStart(2, "0");

    formField = document.getElementById(formFieldId);
    formField.value = dateString;
}


function setToNowDate(formFieldId) {
    var now = new Date();
    dateString = now.getFullYear().toString() + "-" + (now.getMonth() + 1).toString().padStart(2, "0") + "-" + now.getDate().toString().padStart(2, "0")

    formField = document.getElementById(formFieldId);
    formField.value = dateString;
}

function setToNowDateTime(formFieldId) {
    setToNowTimeHM(formFieldId + "_1");
    setToNowDate(formFieldId + "_0");
}