from django.contrib.auth.decorators import login_required
from django.http import HttpResponse
from django.template import loader
from django.utils.decorators import method_decorator
from django.views import View

from management.models import Employment


@method_decorator(login_required, name='dispatch')
class SicknessPeriodDashboardView(View):

    def get(self, request):
        template = loader.get_template("sickness/dashboard.html")
        context = {}
        user = request.user

        employments = Employment.objects.filter(
            profile__user=request.user
        )
        context["employments"] = employments
        return HttpResponse(template.render(context, request))
