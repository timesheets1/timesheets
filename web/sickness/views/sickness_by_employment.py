from django.contrib.auth.decorators import login_required
from django.http import HttpResponse
from django.shortcuts import get_object_or_404, redirect
from django.template import loader
from django.utils.decorators import method_decorator
from django.views import View
from django.contrib import messages
from management.models import Employment
from sickness.forms.sickness_form import SicknessPeriodForm
from sickness.models.sickness_period import SicknessPeriod


@method_decorator(login_required, name='dispatch')
class SicknessByEmploymentView(View):
    def get(self, request, employment_id):
        template = loader.get_template("sickness/sickness_by_employment.html")
        context = {}
        user = request.user

        employment = get_object_or_404(
            Employment,
            id=employment_id,
            profile__user=user,
        )
        context["employment"] = employment

        sickness_periods = SicknessPeriod.objects.filter(
            employment=employment
        )
        context["sickness_periods"] = sickness_periods

        form = SicknessPeriodForm(employment=employment)
        context["form"] = form
        return HttpResponse(template.render(context, request))

    def post(self, request, employment_id):
        template = loader.get_template("sickness/sickness_by_employment.html")
        context = {}
        employment = get_object_or_404(
            Employment,
            id=employment_id,
            profile__user=request.user,
        )
        form = SicknessPeriodForm(request.POST, employment=employment)
        context["employment"] = employment
        if form.is_valid():
            instance = form.save()

            messages.success(request, f"Sickness Period {instance.name} created.")
            return redirect("sickness_by_employment", employment_id)
        else:
            context["form"] = form
            return HttpResponse(template.render(context, request))


@method_decorator(login_required, name='dispatch')
class SicknessByEmploymentUpdateView(View):
    def get(self, request, employment_id, sickness_period_id):
        template = loader.get_template("sickness/sickness_by_employment_edit.html")
        context = {}
        employment = get_object_or_404(
            Employment,
            id=employment_id,
            profile__user=request.user
        )
        context["employment"] = employment
        sickness_period = get_object_or_404(
            SicknessPeriod,
            id=sickness_period_id,
            employment=employment
        )
        context["sickness_period"] = sickness_period

        form = SicknessPeriodForm(instance=sickness_period, employment=employment)
        context["form"] = form
        return HttpResponse(template.render(context, request))

    def post(self, request, employment_id, sickness_period_id):
        template = loader.get_template("sickness/sickness_by_employment_edit.html")
        context = {}
        employment = get_object_or_404(
            Employment,
            id=employment_id,
            profile__user=request.user
        )
        context["employment"] = employment
        sickness_period = get_object_or_404(
            SicknessPeriod,
            id=sickness_period_id,
            employment=employment
        )
        context["sickness_period"] = sickness_period

        form = SicknessPeriodForm(data=request.POST, instance=sickness_period, employment=employment)
        if form.is_valid():
            form.save()
            messages.success(request, "Your changes have been saved.")
            return redirect("sickness_by_employment", employment_id=employment.id)
        else:
            context["form"] = form
            return HttpResponse(template.render(context, request))


@method_decorator(login_required, name='dispatch')
class SicknessPeriodByEmploymentDeleteView(View):
    def post(self, request, employment_id, sickness_period_id):
        employment = get_object_or_404(
            Employment,
            id=employment_id,
            profile__user=request.user
        )
        sickness_period = get_object_or_404(
            SicknessPeriod,
            id=sickness_period_id,
            employment=employment
        )
        sickness_period.delete()

        messages.warning(
            request,
            f'Sickness Period {sickness_period.name} was deleted.'
        )

        return redirect("sickness_by_employment", employment_id=employment.id)
