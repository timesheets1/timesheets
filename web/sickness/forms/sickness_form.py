from django import forms
from django.core.exceptions import ValidationError
from django.utils.translation import gettext_lazy

from sickness.models.sickness_period import SicknessPeriod


class SicknessPeriodForm(forms.ModelForm):
    start = forms.DateField(
        label=gettext_lazy("Start"),
        widget=forms.DateInput(attrs={'type': 'date'}),
    )
    end = forms.DateField(
        label=gettext_lazy("End"),
        widget=forms.DateInput(attrs={'type': 'date'}),
    )

    def __init__(self, *args, **kwargs):
        self.employment = kwargs.pop('employment', None)
        super().__init__(*args, **kwargs)

    def clean(self):
        cleaned_data = super().clean()
        start = cleaned_data.get("start")
        end = cleaned_data.get("end")

        if start and end:
            if end < start:
                raise ValidationError("Start must be before end")

    def save(self, commit=True):
        instance = super().save(commit=False)
        instance.employment = self.employment

        if commit:
            instance.save()
            self.save_m2m()

        return instance

    class Meta:
        model = SicknessPeriod
        exclude = ["employment"]
