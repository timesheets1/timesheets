from django.contrib import admin

from sickness.models.sickness_period import SicknessPeriod


# Register your models here.
@admin.register(SicknessPeriod)
class SicknessPeriodAdmin(admin.ModelAdmin):
    list_display = ["name", "start", "end"]
    list_filter = ["employment__company"]
    date_hierarchy = "start"

