from django.urls import path

from sickness.views.dashboard import SicknessPeriodDashboardView
from sickness.views.sickness_by_employment import SicknessByEmploymentView, SicknessPeriodByEmploymentDeleteView, \
    SicknessByEmploymentUpdateView

urlpatterns = [
    path("", SicknessPeriodDashboardView.as_view(), name="sickness_dashboard"),
    path(
        "sicknessbyemployment/<int:employment_id>/",
        SicknessByEmploymentView.as_view(),
        name="sickness_by_employment"
    ),
    path(
        "sicknessbyemployment/<int:employment_id>/<sickness_period_id>/update",
        SicknessByEmploymentUpdateView.as_view(),
        name="sickness_by_employment_update"
    ), path(
        "sicknessbyemployment/<int:employment_id>/<sickness_period_id>/delete",
        SicknessPeriodByEmploymentDeleteView.as_view(),
        name="sickness_by_employment_delete"
    ),
]
