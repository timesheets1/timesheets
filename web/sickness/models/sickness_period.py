import uuid

from django.db import models
from django.utils.translation import gettext_lazy

from management.models import Employment


class SicknessPeriod(models.Model):
    id = models.UUIDField(verbose_name=gettext_lazy("Id"), primary_key=True, default=uuid.uuid4, editable=False)
    name = models.CharField(verbose_name=gettext_lazy("Name"), max_length=999)
    start = models.DateField(verbose_name=gettext_lazy("Start"), )
    end = models.DateField(verbose_name=gettext_lazy("End"), )
    notes = models.CharField(verbose_name=gettext_lazy("Notes"), max_length=400, blank=True, null=True)
    employment = models.ForeignKey(Employment, verbose_name=gettext_lazy("Employment"), on_delete=models.CASCADE)

    def get_days(self):
        return (self.end - self.start).days + 1

    def __str__(self):
        return self.name

    class Meta:
        ordering = ["-start"]
        verbose_name = gettext_lazy("Sickness Period")
        verbose_name_plural = gettext_lazy("Sickness Periods")
