import datetime as dt
from decimal import Decimal

from django.contrib.auth.decorators import login_required
from django.db.models import Sum, F, Q

from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import redirect, get_object_or_404, render
from django.template import loader
from django.urls import reverse
from django.utils import timezone as tz, timezone

from management.models import Profile, Employment, PublicHoliday
from management.utils import employment_period_integrity_check, overlapping_intervals_integrity_check, \
    contracted_work_time_at, interval_integrity_check
from sickness.models.sickness_period import SicknessPeriod
from .forms import DeleteIntervalForm, IntervalModelFormAdvanced
from .helper import get_employments_for_user, get_information_for_week_display
from .models import Interval, Category, Subcategory


@login_required(login_url='account_login')
def current_week(request):
    now = tz.now()
    current_cw = now.strftime("%V")
    current_year = now.strftime("%G")

    return redirect(week, current_cw=current_cw, current_year=current_year)


@login_required(login_url='account_login')
def current_week_employment(request, employment_id):
    now = tz.now()
    current_cw = now.strftime("%V")
    current_year = now.strftime("%G")
    return redirect(week_employment, current_cw=current_cw, current_year=current_year,
                    employment_id=employment_id)


@login_required(login_url='account_login')
def week_employment(request, employment_id, current_year, current_cw):
    template = loader.get_template('interval/week_employment.html')
    employment = get_object_or_404(Employment, id=employment_id)
    context = get_information_for_week_display(
        user=request.user,
        employment=employment,
        current_year=current_year,
        current_cw=current_cw
    )

    return HttpResponse(template.render(context, request))


@login_required(login_url='account_login')
def week(request, current_year, current_cw):
    template = loader.get_template('interval/week.html')
    context = get_information_for_week_display(user=request.user, employment=None, current_year=current_year,
                                               current_cw=current_cw)

    return HttpResponse(template.render(context, request))


@login_required(login_url='account_login')
def interval_create(request):
    template = loader.get_template('interval/interval_create.html')
    context = {}

    profile = Profile.objects.filter(user=request.user).first()

    context["request_method"] = request.method

    if request.method == "POST":

        form = IntervalModelFormAdvanced(request.POST, profile=profile)

        context["form"] = form

        if form.is_valid():
            form.save()

            return HttpResponseRedirect(reverse("timesheets_today"))
    else:

        form = IntervalModelFormAdvanced(
            initial={
                "start": tz.now(),
                "end": tz.now(),
            },
            profile=profile,
            try_prefill=True
        )
        context["form"] = form
    return HttpResponse(template.render(context, request))


@login_required(login_url='account_login')
def interval_edit(request, interval_id):
    template = loader.get_template('interval/interval_edit.html')
    context = {}
    profile = Profile.objects.filter(user=request.user).first()

    categories = Category.objects.all().order_by('name')
    context['categories'] = categories

    subcategories = Subcategory.objects.all().order_by('name')
    context['subcategories'] = subcategories

    context["request_method"] = request.method

    interval = get_object_or_404(Interval, pk=interval_id, user=request.user)
    context["interval"] = interval

    if request.method == "POST":

        form = IntervalModelFormAdvanced(request.POST, instance=interval, profile=profile)
        context["form"] = form

        if form.is_valid():
            form.save()

            return HttpResponseRedirect(reverse("timesheets_today"))
    else:
        form = IntervalModelFormAdvanced(instance=interval, profile=profile)
        context["form"] = form
        context["category"] = interval.category
        context["subcategory"] = interval.subcategory

    return HttpResponse(template.render(context, request))


@login_required(login_url='account_login')
def interval_delete(request, interval_id):
    template = loader.get_template('interval/interval_delete.html')
    context = {}

    interval = get_object_or_404(Interval, pk=interval_id, user=request.user)
    context["interval"] = interval
    form = DeleteIntervalForm(request.POST, instance=interval)

    if request.method == "POST":
        if form.is_valid():  # checks CSRF
            interval.delete()
            return HttpResponseRedirect(reverse("timesheets_today"))  # wherever to go after deleting

    return HttpResponse(template.render(context, request))


@login_required(login_url='account_login')
def today(request):
    template = loader.get_template('interval/today.html')
    context = {}
    return HttpResponse(template.render(context, request))


@login_required(login_url='account_login')
def hx_interval_create(request):
    template = loader.get_template('partials/interval_create_form.html')
    context = {}
    profile = Profile.objects.filter(user=request.user).first()
    print(request.method)
    if request.method == "GET":
        form = IntervalModelFormAdvanced(
            initial={
                "start": tz.now(),
                "end": tz.now(),
            },
            profile=profile,
            try_prefill=True,
        )
        context["form"] = form
        return HttpResponse(template.render(context, request))

    if request.method == "POST":
        form = IntervalModelFormAdvanced(request.POST, profile=profile)
        if form.is_valid():

            template = loader.get_template("partials/interval_refresh_list.html")
            context = {}
            form.save()
            context["message"] = "Interval created"
            return HttpResponse(template.render(context, request))
        else:
            print(form.errors)
            context["form"] = form
            return HttpResponse(template.render(context, request))

    else:
        return '<span class="text-red-600">Only get is allowed</span>'


@login_required(login_url='account_login')
def hx_interval_update(request, interval_id):
    template = loader.get_template('partials/interval_update_form.html')
    context = {}

    interval = get_object_or_404(Interval, pk=interval_id, user=request.user)
    context["interval"] = interval

    profile = Profile.objects.filter(user=request.user).first()
    if request.method == "GET":
        form = IntervalModelFormAdvanced(
            instance=interval,
            profile=profile
        )
        context["form"] = form
        return HttpResponse(template.render(context, request))

    if request.method == "POST":
        form = IntervalModelFormAdvanced(instance=interval, data=request.POST, profile=profile)
        if form.is_valid():
            template = loader.get_template("partials/interval_refresh_list.html")
            context = {}

            form.save()
            context["message"] = "Interval updated"
            return HttpResponse(template.render(context, request))
        else:
            print(form.errors)
            context["form"] = form
            return HttpResponse(template.render(context, request))

    else:
        return '<span class="text-red-600">Only get is allowed</span>'


@login_required(login_url='account_login')
def hx_interval_delete(request, interval_id):
    template = loader.get_template('partials/interval_create_form.html')
    context = {}
    profile = Profile.objects.filter(user=request.user).first()
    print(request.method)
    if request.method == "POST":

        interval = get_object_or_404(
            Interval,
            id=interval_id
        )
        interval.delete()

        template = loader.get_template("partials/interval_refresh_list.html")
        context = {}
        context["message"] = "Interval deleted"

        return HttpResponse(template.render(context, request))

    else:
        return '<span class="text-red-600">Only get is allowed</span>'


@login_required(login_url='account_login')
def hx_get_todays_intervals(request):
    template = loader.get_template('partials/interval_list.html')
    context = {}

    today = timezone.now().date()

    employments = Employment.objects.filter(profile__user=request.user)

    sickness_periods = SicknessPeriod.objects.filter(
        start__lte=today,
        end__gte=today,
        employment__in=employments,
    ).order_by("employment__company", "start")
    context["sickness_periods"] = sickness_periods

    company_ids = employments.values_list("company_id", flat=True)

    public_holidays = PublicHoliday.objects.filter(
        company_id__in=company_ids,
        date=today,
    )
    context["public_holidays"] = public_holidays

    intervals = Interval.objects.filter(
        Q(start__date=today) |
        Q(end__date=today),
        user=request.user,
    ).order_by("-start")

    context["intervals"] = intervals
    total_work_time_today = intervals.aggregate(duration=Sum(F("end") - F("start")))["duration"]
    if intervals:
        total_work_time_today = total_work_time_today.total_seconds() / (60 * 60)
    else:
        total_work_time_today = Decimal(0)
    context["total_work_time_today"] = total_work_time_today

    employments = get_employments_for_user(user=request.user, reference_date=timezone.now().date())
    context["employments"] = employments

    employment_period_issues = []
    contracted_time_per_employment = []
    for employment in employments:
        employment_period_issues.append({
            "employment": employment,
            "issues": employment_period_integrity_check(employment)
        })

        worked_time = intervals.filter(company=employment.company).aggregate(duration=Sum(F("end") - F("start")))[
            "duration"]
        if worked_time:
            worked_time = Decimal(worked_time.total_seconds() / (60 * 60))
        else:
            worked_time = Decimal(0)

        contracted_time = contracted_work_time_at(employment=employment, date=timezone.now().date())

        progress = Decimal(100)
        if contracted_time != Decimal(0):
            progress = 100 * worked_time / contracted_time

        contracted_time_per_employment.append({
            "employment": employment,
            "contracted_time": contracted_time,
            "worked_time": worked_time,
            "remaining_time": contracted_time - worked_time,
            "progress": progress,
            "progress_str": progress.__str__().replace(",", "."),
        })
    context["employment_period_issues"] = employment_period_issues
    context["contracted_time_per_employment"] = contracted_time_per_employment
    interval_issues = len(overlapping_intervals_integrity_check(request.user)) + len(
        interval_integrity_check(request.user))
    context["interval_issues"] = interval_issues
    return HttpResponse(template.render(context, request))
