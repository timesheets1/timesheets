from django.core.management.base import BaseCommand

from timesheets.models import Category


class Command(BaseCommand):
	help = 'initializes the application by inserting default categories and subcategries'

	def handle(self, *args, **options):
		catWorkTime = Category(name='Work Time', description='Time working for a project', workingTime=True)
		catWorkTime.save()
		self.stdout.write('Included Category ' + catWorkTime.name)

		catAdmin = Category(name='Admin', description='Time working for general business reasons', workingTime=True)
		catAdmin.save()
		self.stdout.write('Included Category ' + catAdmin.name)

		catTravelTime = Category(name='Travel Time', description='Time travelling for a project', workingTime=True)
		catTravelTime.save()
		self.stdout.write('Included Category ' + catTravelTime.name)

		catTraining = Category(name='Training Time', description='Time spent training', workingTime=True)
		catTraining.save()
		self.stdout.write('Included Category ' + catTraining.name)
