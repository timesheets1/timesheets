import logging
from datetime import datetime as dt

from django.contrib.auth.models import User, Group, Permission
from django.core.management.base import BaseCommand

from timesheets.models import Category, Subcategory, Interval


class Command(BaseCommand):
    help = 'initializes the application by inserting default categories and subcategries'

    def createGroupsAndPermissions(self, GROUPS, MODELS, PERMISSIONS):
        """
        Helper function to generate all combinations of group names, models
        and permissions, without having to explicitly state every combination.
        """
        for group in GROUPS:
            new_group, created = Group.objects.get_or_create(name=group)
            for model in MODELS:
                for permission in PERMISSIONS:
                    name = 'Can {} {}'.format(permission, model)
                    print("Creating {}".format(name))
                    try:
                        model_add_perm = Permission.objects.get(name=name)
                    except Permission.DoesNotExist:
                        logging.warning("Permission not found with name '{}'.".format(name))
                        continue
                    new_group.permissions.add(model_add_perm)

    def handle(self, *args, **options):

        # groups
        GROUPS = ['timesheet_users']
        MODELS = ['Category', 'Subcategory']
        PERMISSIONS = ['view', ]  # For now only view permission by default for all, others include add, delete, change
        self.createGroupsAndPermissions(GROUPS, MODELS, PERMISSIONS)

        GROUPS = ['timesheet_users']
        MODELS = ['Interval']
        PERMISSIONS = ['add', 'delete',
                       'change']  # For now only view permission by default for all, others include add, delete, change
        self.createGroupsAndPermissions(GROUPS, MODELS, PERMISSIONS);

        # users
        defaultUser = User.objects.create_user('defaultUser', 'default@user.com', 'default', is_staff=True)
        defaultUser.save()
        my_group = Group.objects.get(name='timesheet_users')
        my_group.user_set.add(defaultUser)

        otherUser = User.objects.create_user('otherUser', 'other@user.com', 'other', is_staff=True)
        otherUser.save()
        my_group = Group.objects.get(name='timesheet_users')
        my_group.user_set.add(otherUser)

        superUser = User.objects.create_user('superUser', 'super@user.com', 'super', is_staff=True, is_superuser=True)
        superUser.save()

        # categories
        catWorkTime = Category(name='Work Time', description='Time working for a project', working_time=True,
                               has_subcategory=True)
        catWorkTime.save()
        self.stdout.write('Included Category ' + catWorkTime.name)

        catAdmin = Category(name='Admin', description='Time working for general business reasons', working_time=True,
                            has_subcategory=False)
        catAdmin.save()
        self.stdout.write('Included Category ' + catAdmin.name)

        catTravelTime = Category(name='Travel Time', description='Time travelling for a project', working_time=True,
                                 has_subcategory=True)
        catTravelTime.save()
        self.stdout.write('Included Category ' + catTravelTime.name)

        catTraining = Category(name='Training Time', description='Time spent training', working_time=True,
                               has_subcategory=False)
        catTraining.save()
        self.stdout.write('Included Category ' + catTraining.name)

        catOnCall = Category(name='On Call Time', description='Time being on call.', working_time=False,
                             has_subcategory=True)
        catOnCall.save()
        self.stdout.write('Included Category ' + catOnCall.name)

        # subcategories
        subcatA = Subcategory(name='SPM', description='Sleek Payment methods')
        subcatA.save()
        self.stdout.write('Included Subcategory ' + subcatA.name)

        subcatB = Subcategory(name='CMS', description='Development of a content management system.')
        subcatB.save()
        self.stdout.write('Included Subcategory ' + subcatB.name)

        subcatC = Subcategory(name='TeXpert', description='Consulting and training in LaTeX')
        subcatC.save()
        self.stdout.write('Included Subcategory ' + subcatC.name)

        currentCW = dt.now().strftime('%W')
        currentYear = dt.now().year

        for i in range(1, 5):
            intervalA = Interval(
                start=dt.strptime(str(currentYear) + '-' + str(currentCW) + '-' + str(i) + '_6:00', '%Y-%W-%w_%H:%M'),
                end=dt.strptime(str(currentYear) + '-' + str(currentCW) + '-' + str(i) + '_8:00', '%Y-%W-%w_%H:%M'),
                category=catTravelTime,
                subcategory=subcatA,
                notes='auto generated test Interval',
                user=defaultUser,
            )
            intervalA = Interval(
                start=dt.strptime(str(currentYear) + '-' + str(currentCW) + '-' + str(i) + '_8:00', '%Y-%W-%w_%H:%M'),
                end=dt.strptime(str(currentYear) + '-' + str(currentCW) + '-' + str(i) + '_11:30', '%Y-%W-%w_%H:%M'),
                category=catWorkTime,
                subcategory=subcatA,
                notes='auto generated test Interval',
                user=defaultUser,
            )
            intervalA.save()
            self.stdout.write('Included Interval' + str(intervalA))
            intervalB = Interval(
                start=dt.strptime(str(currentYear) + '-' + str(currentCW) + '-' + str(i) + '_12:30', '%Y-%W-%w_%H:%M'),
                end=dt.strptime(str(currentYear) + '-' + str(currentCW) + '-' + str(i) + '_17:15', '%Y-%W-%w_%H:%M'),
                category=catAdmin,
                subcategory=None,
                notes='auto generated test Interval',
                user=defaultUser,
            )
            intervalB.save()
            self.stdout.write('Included Interval' + str(intervalB))

        intervalA = Interval(
            start=dt.strptime(str(currentYear) + '-' + str(currentCW) + '-' + str(5) + '_7:40', '%Y-%W-%w_%H:%M'),
            end=dt.strptime(str(currentYear) + '-' + str(currentCW) + '-' + str(5) + '_11:30', '%Y-%W-%w_%H:%M'),
            category=catTraining,
            subcategory=None,
            notes='auto generated test Interval',
            user=defaultUser,
        )
        intervalA.save()
        self.stdout.write('Included Interval' + str(intervalA))
        intervalB = Interval(
            start=dt.strptime(str(currentYear) + '-' + str(currentCW) + '-' + str(5) + '_12:30', '%Y-%W-%w_%H:%M'),
            end=dt.strptime(str(currentYear) + '-' + str(currentCW) + '-' + str(5) + '_15:15', '%Y-%W-%w_%H:%M'),
            category=catWorkTime,
            subcategory=subcatB,
            notes='auto generated test Interval',
            user=defaultUser,
        )
        self.stdout.write('Included Interval' + str(intervalA))
        intervalB = Interval(
            start=dt.strptime(str(currentYear) + '-' + str(currentCW) + '-' + str(5) + '_15:15', '%Y-%W-%w_%H:%M'),
            end=dt.strptime(str(currentYear) + '-' + str(currentCW) + '-' + str(5) + '_17:00', '%Y-%W-%w_%H:%M'),
            category=catTravelTime,
            subcategory=subcatB,
            notes='auto generated test Interval',
            user=defaultUser,
        )
        intervalB.save()
        self.stdout.write('Included Interval' + str(intervalB))
