from django.utils import timezone

from django import forms
from django.contrib.auth.models import User
from django.core.exceptions import ValidationError
from django.db.models import Q, F
from django.utils.translation import gettext_lazy

from management.models import Employment
from timesheets.helper import get_companies_for_user
from timesheets.models import Category, Subcategory, Interval
from webTimesheets.custom_form_fields import DateInput


class IntervalModelFormAdvanced(forms.ModelForm):
    start = forms.SplitDateTimeField(
        label=gettext_lazy("Start"),
        widget=forms.SplitDateTimeWidget(
            date_format="%Y-%m-%d",
            time_format="%H:%M",
            date_attrs={'type': 'date'},
            time_attrs={'type': 'time'},
        ),
        required=True
    )
    end = forms.SplitDateTimeField(
        label=gettext_lazy("End"),
        widget=forms.SplitDateTimeWidget(
            date_format="%Y-%m-%d",
            time_format="%H:%M",
            date_attrs={'type': 'date'},
            time_attrs={'type': 'time'},
        ),
        required=True
    )

    category = forms.ModelChoiceField(
        label=gettext_lazy("Category"),
        queryset=Category.objects.filter(company__isnull=True, bookable=True),
        required=True,
    )
    subcategory = forms.ModelChoiceField(
        label=gettext_lazy("Subcategory"),
        queryset=Subcategory.objects.filter(company__isnull=True, bookable=True),
        required=False,
    )
    notes = forms.CharField(
        label=gettext_lazy("Notes"),
        required=False
    )
    issue_url = forms.CharField(
        label=gettext_lazy("Issue URL"),
        required=False,
    )
    issue_reference = forms.CharField(
        label=gettext_lazy("Issue Reference"),
        required=False,
    )

    def __init__(self, *args, **kwargs):
        self.profile = kwargs.pop('profile', None)
        self.try_prefill = kwargs.pop('try_prefill', False)
        today = timezone.now().date()
        super().__init__(*args, **kwargs)
        latest_boked_interval = None
        if self.profile:

            self.user = self.profile.user

            # overwrite queryset
            available_categories = Category.objects.filter(company__isnull=True, bookable=True)
            available_subcategories = Subcategory.objects.filter(company__isnull=True, bookable=True)
            companies = get_companies_for_user(self.profile.user)
            if companies:
                employments_company_ids = Employment.objects.filter(
                    Q(end__gte=today) | Q(end__isnull=True),
                    start__lte=today,
                    profile=self.profile,
                ).values_list("company__id", flat=True)
                available_categories = Category.objects.filter(
                    Q(company__isnull=True) |
                    Q(company__in=employments_company_ids),
                    bookable=True
                )

                available_subcategories = Subcategory.objects.filter(
                    Q(company__isnull=True) |
                    Q(company__in=employments_company_ids),
                    bookable=True
                )
                self.fields['category'].queryset = available_categories
                self.fields['subcategory'].queryset = available_subcategories

            # overwrite defaults
            latest_boked_interval = Interval.objects.filter(
                user=self.profile.user,
                category__in=available_categories,
                subcategory__in=available_subcategories,
            ).order_by("start").last()
        if latest_boked_interval and self.try_prefill:
            self.fields['category'].initial = latest_boked_interval.category
            self.fields['subcategory'].initial = latest_boked_interval.subcategory
            self.fields['notes'].initial = latest_boked_interval.notes
            self.fields['issue_url'].initial = latest_boked_interval.issue_url
            self.fields['issue_reference'].initial = latest_boked_interval.issue_reference

    def clean(self):
        cleaned_data = super().clean()
        category = cleaned_data.get("category")
        subcategory = cleaned_data.get("subcategory")

        if category:
            if subcategory:
                if category.company != subcategory.company:
                    raise ValidationError("Category and Subcategory must belong to the same company (or none).")

    def save(self, commit=True):
        instance = super().save(commit=False)
        instance.user = self.profile.user
        instance.company = instance.category.company
        if commit:
            instance.save()
            self.save_m2m()

        return instance

    class Meta:
        model = Interval
        exclude = ['user', 'company']


class DeleteIntervalForm(forms.ModelForm):
    class Meta:
        model = Interval
        fields = []
