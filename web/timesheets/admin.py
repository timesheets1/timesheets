from django.contrib import admin

from .models import Category, Subcategory, Interval, Company


@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin):
    list_display = ('name', 'description', 'working_time', 'company', 'bookable', 'hourly_rate')
    list_filter = ('company', 'working_time')
    search_fields = ['name', 'description']


@admin.register(Subcategory)
class SubCategoryAdmin(admin.ModelAdmin):
    list_display = ('name', 'description', 'company', 'bookable', 'hourly_rate')
    search_fields = ['name', 'description']


@admin.register(Interval)
class IntervalAdmin(admin.ModelAdmin):
    list_display = ('start', 'end', 'get_duration', 'category', 'subcategory', 'notes', 'user', 'company')
    list_filter = ('category', 'subcategory', 'user', 'company')
    search_fields = ['notes', 'category', 'subcategory', 'user']
    date_hierarchy = 'start'


@admin.register(Company)
class CompanyAdmin(admin.ModelAdmin):
    list_display = ["name"]
