from django import template

register = template.Library()


@register.filter
def decimal_hours(value):
	"""
	Accepts a datetime.timedelta object and returns a double
	specifying the time in hours.
	"""
	return value.total_seconds() / 3600


