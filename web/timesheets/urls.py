from django.urls import path

from . import views

urlpatterns = [
    path('', views.today, name='timesheets_today'),
    path('current_week', views.current_week, name='timesheets_current_week'),
    path('current_week_employment/<int:employment_id>', views.current_week_employment,
         name='timesheets_current_week_employment'),
    path('<int:employment_id>/<int:current_year>/<int:current_cw>/', views.week_employment, name='week_employment'),
    path('<int:current_year>/<int:current_cw>/', views.week, name='week'),
    path('interval/create/', views.interval_create, name="timesheets_create_interval"),
    path('interval/<interval_id>/edit/', views.interval_edit, name="timesheets_edit_interval"),
    path('interval/<interval_id>/delete/', views.interval_delete, name="timesheets_delete_interval"),
]

htmx_urlpatterns = [
    path('hx/intervals_today', views.hx_get_todays_intervals, name="hx_intervals_today"),
    path('hx/interval/create', views.hx_interval_create, name="hx_interval_create"),
    path('hx/interval/<int:interval_id>/delete', views.hx_interval_delete, name="hx_interval_delete"),
    path('hx/interval/<int:interval_id>/update', views.hx_interval_update, name="hx_interval_update"),
]

urlpatterns += htmx_urlpatterns
