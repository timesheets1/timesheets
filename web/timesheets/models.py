from django.contrib.auth.models import User
from django.db import models
from django.utils.translation import gettext_lazy

from management.models import Profile, Company


class Category(models.Model):
    name = models.CharField(verbose_name=gettext_lazy("Name"), max_length=200)
    description = models.CharField(verbose_name=gettext_lazy("Description"), max_length=200, blank=True, default='')
    working_time = models.BooleanField(verbose_name=gettext_lazy("Working Time"), default=True)
    company = models.ForeignKey(Company, verbose_name=gettext_lazy("Company"), null=True, blank=True,
                                on_delete=models.CASCADE)
    bookable = models.BooleanField(verbose_name=gettext_lazy("Bookable"), default=True)
    exclusive = models.BooleanField(verbose_name=gettext_lazy("Exclusive"), default=True,
                                    help_text="Controls if intervals in this category are relevant for integrity check.")
    hourly_rate = models.DecimalField(verbose_name=gettext_lazy("Hourly Rate"), decimal_places=2, max_digits=7,
                                      null=True, blank=True)

    def __str__(self):
        name = self.name
        if self.company:
            name += " - " + self.company.name
        return name

    class Meta:
        verbose_name = gettext_lazy("Category")
        verbose_name_plural = gettext_lazy("Categories")
        ordering = ["-company", "name"]


class Subcategory(models.Model):
    name = models.CharField(verbose_name=gettext_lazy("Name"), max_length=200)
    description = models.CharField(verbose_name=gettext_lazy("Description"), max_length=200, blank=True, default='')
    company = models.ForeignKey(Company, verbose_name=gettext_lazy("Company"), null=True, blank=True,
                                on_delete=models.CASCADE)
    bookable = models.BooleanField(verbose_name=gettext_lazy("Bookable"), default=True)
    hourly_rate = models.DecimalField(verbose_name=gettext_lazy("Hourly Rate"), decimal_places=2, max_digits=7,
                                      null=True, blank=True)

    def __str__(self):
        name = self.name
        if self.company:
            name += " - " + self.company.name
        return name

    class Meta:
        verbose_name = gettext_lazy("Subcategory")
        verbose_name_plural = gettext_lazy("Subcategories")
        ordering = ["-company", "name"]


class Interval(models.Model):
    start = models.DateTimeField(verbose_name=gettext_lazy("Start"))
    end = models.DateTimeField(verbose_name=gettext_lazy("End"), )
    category = models.ForeignKey(Category, verbose_name=gettext_lazy("Category"), on_delete=models.CASCADE)
    subcategory = models.ForeignKey(Subcategory, verbose_name=gettext_lazy("Subcategory"), on_delete=models.CASCADE,
                                    blank=True, null=True)
    notes = models.CharField(max_length=400, verbose_name=gettext_lazy("Notes"), blank=True, null=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE, verbose_name=gettext_lazy("User"), )
    company = models.ForeignKey(Company, verbose_name=gettext_lazy("Company"), null=True, blank=True,
                                on_delete=models.CASCADE)
    issue_url = models.URLField(blank=True, null=True, verbose_name=gettext_lazy("Issue URL"), )
    issue_reference = models.CharField(max_length=400, default="", verbose_name=gettext_lazy("Issue Reference"))

    def get_duration(self):
        return self.end - self.start

    def __str__(self):
        return self.start.strftime("%Y-%m-%d") + " - " + str(self.get_duration().total_seconds() / (60 * 60)) + "h -- "

    # +self.user.name

    class Meta:
        verbose_name = gettext_lazy("Interval")
        verbose_name_plural = gettext_lazy("Intervals")
