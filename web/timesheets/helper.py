from decimal import Decimal

import structlog
from django.db.models import Sum, F, Q
from django.utils import timezone
import datetime as dt

from django.utils.translation import gettext_lazy, gettext

from holidays.models import HolidayDay
from management.models import Profile, Employment, Company, ContractPeriod, PublicHoliday
from management.utils import contracted_work_time_at
from sickness.models.sickness_period import SicknessPeriod
from timesheets.models import Interval, Category, Subcategory
from timesheets.utils import daterange


def get_companies_for_user(user):
    profile = Profile.objects.filter(user=user).first()

    employments = Employment.objects.filter(profile=profile).order_by("order")

    companies = []
    for employment in employments:
        companies.append(employment.company.id)

    # make queryset
    companies = Company.objects.filter(id__in=companies)

    return companies


def get_employments_for_user(user, reference_date=None):
    profile = Profile.objects.filter(user=user).first()

    employments = Employment.objects.filter(profile=profile).order_by("order")

    if reference_date:
        employments = employments.filter(
            Q(end__isnull=True) | Q(end__gte=reference_date),
            start__lte=reference_date,
        )

    return employments


def get_information_for_week_display(user, employment, current_year, current_cw):
    logger = structlog.getLogger("timesheets")
    times = {}

    begin = timezone.now()

    context = {}
    context["employment"] = employment

    employments = get_employments_for_user(user=user)
    context["employments"] = employments

    now = timezone.now()
    if employment:
        company = employment.company
    else:
        company = None

    context["current_cw"] = current_cw

    username = user.get_username()
    context["username"] = username

    firstDayOfThisWeek = dt.datetime.strptime(str(current_year) + "_" + str(current_cw) + "_" + str(1), "%G_%V_%u")
    firstDayOfNextWeek = dt.datetime.strptime(str(current_year) + "_" + str(current_cw) + "_" + str(1),
                                              "%G_%V_%u") + dt.timedelta(days=7)
    firstDayOfLastWeek = dt.datetime.strptime(str(current_year) + "_" + str(current_cw) + "_" + str(1),
                                              "%G_%V_%u") + dt.timedelta(days=-7)
    lastDayOfThisWeek = dt.datetime.strptime(str(current_year) + "_" + str(current_cw) + "_" + str(7), "%G_%V_%u")

    firstDayOfThisWeek = timezone.make_aware(firstDayOfThisWeek, timezone=timezone.get_current_timezone()).date()
    firstDayOfNextWeek = timezone.make_aware(firstDayOfNextWeek, timezone=timezone.get_current_timezone()).date()
    firstDayOfLastWeek = timezone.make_aware(firstDayOfLastWeek, timezone=timezone.get_current_timezone()).date()
    lastDayOfThisWeek = timezone.make_aware(lastDayOfThisWeek, timezone=timezone.get_current_timezone()).date()

    previous_week = firstDayOfLastWeek.strftime("%V")
    previous_year = firstDayOfLastWeek.strftime("%G")

    next_week = firstDayOfNextWeek.strftime("%V")
    next_year = firstDayOfNextWeek.strftime("%G")

    context["previous_week"] = previous_week
    context["previous_year"] = previous_year
    context["next_week"] = next_week
    context["next_year"] = next_year

    # sickness periods

    sickness_periods = SicknessPeriod.objects.filter(
        start__lte=lastDayOfThisWeek,
        end__gte=firstDayOfThisWeek,
        employment=employment,
    ).order_by("start")
    context["sickness_periods"] = sickness_periods

    times["first steps"] = (timezone.now() - begin).total_seconds()
    begin = timezone.now()

    # work time of this week
    interval_list = Interval.objects.filter(
        user=user,
        start__date__lte=lastDayOfThisWeek,
        start__date__gte=firstDayOfThisWeek
    )

    total_booked_time_this_week = interval_list
    if company:
        total_booked_time_this_week = total_booked_time_this_week.filter(company=company)
    else:
        total_booked_time_this_week = total_booked_time_this_week.filter(company__isnull=True)

    if len(total_booked_time_this_week) > 0:
        total_booked_time_this_week = total_booked_time_this_week.aggregate(duration=Sum(F("end") - F("start")))
    context["total_booked_time_this_week"] = total_booked_time_this_week

    total_work_time_this_week = interval_list.filter(category__working_time=True)
    if company:
        total_work_time_this_week = total_work_time_this_week.filter(company=company)
    else:
        total_work_time_this_week = total_work_time_this_week.filter(company__isnull=True)

    if len(total_work_time_this_week) > 0:
        total_work_time_this_week = total_work_time_this_week.aggregate(duration=Sum(F("end") - F("start")))
    context["total_work_time_this_week"] = total_work_time_this_week

    current_work_hours_period = ContractPeriod.objects.filter(employment=employment, start__lte=now,
                                                              end__gte=now).first()
    # try to see if there is an open ended Period that includes today
    if not current_work_hours_period:
        current_work_hours_period = ContractPeriod.objects.filter(employment=employment, start__lte=now,
                                                                  end=None).first()

    # expected amount for each day
    dates = daterange(firstDayOfThisWeek, lastDayOfThisWeek)
    expected_amount = []
    for date in dates:
        expected_amount.append(contracted_work_time_at(employment=employment, date=date))

    context["expected_amount"] = [float(number) for number in expected_amount]

    required_work_time_this_week = sum(expected_amount)
    context["required_work_time_this_week"] = required_work_time_this_week

    work_time_progress_percent = None
    if total_work_time_this_week and required_work_time_this_week:
        work_time_progress_percent = 100 * (
            Decimal(total_work_time_this_week["duration"].total_seconds() / 60 / 60)) / required_work_time_this_week
        context["work_time_progress_percent"] = work_time_progress_percent

    times["key figures this week"] = (timezone.now() - begin).total_seconds()
    begin = timezone.now()

    # calculate total over hours at the end of a given week!

    # sum all duration of intervals(this week or before) that count towards worktime. workedHours
    # calculate the difference and save it.
    total_work_time_global = Interval.objects.filter(
        user=user,
        start__date__lte=lastDayOfThisWeek,
        category__working_time=True,
    )
    if employment:
        total_work_time_global = total_work_time_global.filter(start__date__gte=employment.start)

        if employment.usage_start:
            total_work_time_global = total_work_time_global.filter(
                start__date__gte=employment.usage_start,
            )

    if company:
        total_work_time_global = total_work_time_global.filter(company=company)
    else:
        total_work_time_global = total_work_time_global.filter(company__isnull=True)

    total_work_time_global = total_work_time_global.aggregate(duration=Sum(F("end") - F("start")))["duration"]

    times["global work time"] = (timezone.now() - begin).total_seconds()
    begin = timezone.now()

    times["overtime old"] = (timezone.now() - begin).total_seconds()
    begin = timezone.now()
    ##################################
    ###### overtime new ##############
    ##################################

    if employment:
        required_work_time = 0

        for period in ContractPeriod.objects.filter(employment=employment):

            # the reference period starts either at employment start or at usage start
            reference_start_date = period.employment.start
            reference_end_date = timezone.now().date()
            # reference start date conditions
            if employment.start > reference_start_date:
                reference_start_date = employment.start

            if period.start > reference_start_date:
                reference_start_date = period.start

            if employment.usage_start and employment.usage_start > reference_start_date:
                reference_start_date = employment.usage_start

            # reference end date conditions
            if employment.end and employment.end < reference_end_date:
                reference_end_date = employment.end
            if period.end and period.end < reference_end_date:
                reference_end_date = period.end
            if lastDayOfThisWeek < reference_end_date:
                reference_end_date = lastDayOfThisWeek

            logger.info(
                "get_information_for_week_display overtime",
                reference_start_date=reference_start_date,
                reference_end_date=reference_end_date
            )
            dates = daterange(reference_start_date, reference_end_date)

            week_day_counter = {
                "0": 0,  # Sunday
                "1": 0,  # Monday
                "2": 0,
                "3": 0,
                "4": 0,
                "5": 0,
                "6": 0,  # Saturday
            }

            for day in dates:
                w = day.strftime("%w")
                week_day_counter[w] = week_day_counter[w] + 1

            # approved holidays
            ## for every holiday day in the respective daterange, remove its weight from the counter of the weekday
            holiday_days = HolidayDay.objects.filter(
                employment=employment,
                date__gte=reference_start_date,
                date__lte=reference_end_date,
                holidayEvent__approved=True
            )
            for day in holiday_days:
                w = day.date.strftime("%w")
                week_day_counter[w] = week_day_counter[w] - day.weight

            public_holiday_days = PublicHoliday.objects.filter(
                company=employment.company,
                date__gte=reference_start_date,
                date__lte=reference_end_date
            )
            for public_holiday_day in public_holiday_days:
                w = public_holiday_day.date.strftime("%w")
                week_day_counter[w] = week_day_counter[w] - public_holiday_day.weight

            # sickness periods
            # for every sickness day, remove one from the week day counter

            sickness_periods = SicknessPeriod.objects.filter(
                end__gte=reference_start_date,
                start__lte=reference_end_date,
            )
            for sickness_period in sickness_periods:
                sick_days = daterange(sickness_period.start, sickness_period.end)
                for sick_day in sick_days:
                    if reference_start_date <= sick_day <= reference_end_date:
                        w = sick_day.strftime("%w")
                        if period.sunday and w == "0":
                            week_day_counter[w] = week_day_counter[w] - 1
                        if period.monday and w == "1":
                            week_day_counter[w] = week_day_counter[w] - 1
                        if period.tuesday and w == "2":
                            week_day_counter[w] = week_day_counter[w] - 1
                        if period.wednesday and w == "3":
                            week_day_counter[w] = week_day_counter[w] - 1
                        if period.thursday and w == "4":
                            week_day_counter[w] = week_day_counter[w] - 1
                        if period.friday and w == "5":
                            week_day_counter[w] = week_day_counter[w] - 1
                        if period.saturday and w == "6":
                            week_day_counter[w] = week_day_counter[w] - 1

            logger.info(
                "get_information_for_week_display overtime",
                week_day_counter=week_day_counter,
            )

            average_worktime_per_day = period.work_hours_amount / period.get_working_days_per_week()

            if period.sunday:
                required_work_time += week_day_counter["0"] * average_worktime_per_day
            if period.monday:
                required_work_time += week_day_counter["1"] * average_worktime_per_day
            if period.tuesday:
                required_work_time += week_day_counter["2"] * average_worktime_per_day
            if period.wednesday:
                required_work_time += week_day_counter["3"] * average_worktime_per_day
            if period.thursday:
                required_work_time += week_day_counter["4"] * average_worktime_per_day
            if period.friday:
                required_work_time += week_day_counter["5"] * average_worktime_per_day
            if period.saturday:
                required_work_time += week_day_counter["6"] * average_worktime_per_day

        total_work_time_global_hours = 0
        if total_work_time_global:
            total_work_time_global_hours = total_work_time_global.total_seconds() / (60 * 60)
        overtime = Decimal(total_work_time_global_hours) - required_work_time

        context["overtime_at_end_of_current_week_new"] = overtime

        times["overtime new"] = (timezone.now() - begin).total_seconds()
        begin = timezone.now()
    # get list of intervals for a given week as reference
    if company:
        interval_list = interval_list.filter(company=company)
    else:
        interval_list = interval_list.filter(company__isnull=True)
    interval_list = interval_list.order_by('start')
    context["interval_list"] = interval_list
    # q = interval_list.values()

    # calculate weekly statistics.
    days_of_calendar_week = []
    for i in range(1, 8):
        days_of_calendar_week.append(
            dt.datetime.strptime(str(current_year) + "_" + str(current_cw) + "_" + str(i), "%G_%V_%u"))
    context["days_of_calendar_week"] = days_of_calendar_week
    categories_in_week_without_subcat = []
    categories_all = Category.objects.all()
    for cat in categories_all:
        intervals_in_cat = interval_list.filter(category=cat, subcategory__isnull=True)
        intervals_in_cat = intervals_in_cat.distinct()
        if len(intervals_in_cat) > 0:
            categories_in_week_without_subcat.append(cat)
    context["categories_in_week_without_subcat"] = categories_in_week_without_subcat

    categories_in_week_with_subcat = []
    categories_all = Category.objects.all()
    for cat in categories_all:
        intervals_in_cat = interval_list.filter(category=cat)

        intervals_in_cat = intervals_in_cat.distinct()
        if len(intervals_in_cat) > 0:
            categories_in_week_with_subcat.append(cat)
    context["categories_in_week_with_subcat"] = categories_in_week_with_subcat

    subcategories_in_week_with_subcat = []
    subcategories_all = Subcategory.objects.all()
    for subcat in subcategories_all:
        intervals_in_subcat = interval_list.filter(subcategory=subcat)

        intervals_in_subcat = intervals_in_subcat.distinct()
        if len(intervals_in_subcat) > 0:
            subcategories_in_week_with_subcat.append(subcat)
    context["subcategories_in_week_with_subcat"] = subcategories_in_week_with_subcat

    # get statistics based on work time directly associated with a Subcategory
    week_time_sub_cat = Interval.objects.filter(user=user, start__year=current_year,
                                                start__week=current_cw, subcategory__isnull=False)
    if company:
        week_time_sub_cat = week_time_sub_cat.filter(company=company)
    else:
        week_time_sub_cat = week_time_sub_cat.filter(company__isnull=True)
    week_time_sub_cat = week_time_sub_cat.values('category__name', 'subcategory__name', 'start__date') \
        .annotate(duration=Sum(F("end") - F("start")))
    context["week_time_sub_cat"] = week_time_sub_cat
    week_time_sub_cat_list = list(week_time_sub_cat)  # evaluate the query set.

    # add missing entries for days without intervals with a duration of 0
    for cat in categories_in_week_with_subcat:
        for subcat in subcategories_in_week_with_subcat:
            for date in days_of_calendar_week:
                contains = False
                for element in week_time_sub_cat_list:
                    if ((subcat.name in element.values() and cat.name in element.values()) and (
                            date.date() in element.values())):
                        contains = True
                if not contains:
                    week_time_sub_cat_list.append(
                        {'category__name': cat.name, 'subcategory__name': subcat.name, 'start__date': date.date(),
                         'duration': dt.timedelta(seconds=0)})

    week_time_sub_cat_list = sorted(week_time_sub_cat_list,
                                    key=lambda k: (k['start__date'], k['category__name'], k['subcategory__name']))
    context["week_time_sub_cat_list"] = week_time_sub_cat_list

    # get statistics based on work time not associated with a Subcategory
    week_time_cat = week_time_sub_cat = Interval.objects.filter(user=user, start__year=current_year,
                                                                start__week=current_cw, subcategory__isnull=True)

    week_time_cat = week_time_cat.values('category__name', 'start__date') \
        .annotate(duration=Sum(F("end") - F("start")))
    context["week_time_cat"] = week_time_cat
    # 'fillna' week_time_cat with duration=0 values if there is no information for the
    # given combination of Category and date.
    week_time_cat_list = list(week_time_cat)  # evaluate the query set.
    # add missing entries for days without intervals with a duration of 0
    for cat in categories_in_week_without_subcat:
        for date in days_of_calendar_week:
            contains = False
            for element in week_time_cat_list:
                if (cat.name in element.values()) and (date.date() in element.values()):
                    contains = True
            if not contains:
                week_time_cat_list.append(
                    {'category__name': cat.name, 'start__date': date.date(), 'duration': dt.timedelta(seconds=0)})

    week_time_cat_list = sorted(week_time_cat_list, key=lambda k: (k['start__date'], k['category__name']))
    context["week_time_cat_list"] = week_time_cat_list
    # generate plot

    weekdays = [
        gettext("Mo"),
        gettext("Tu"),
        gettext("We"),
        gettext("Th"),
        gettext("Fr"),
        gettext("Sa"),
        gettext("Su"),
    ]
    context["weekdays"] = weekdays

    title = gettext('Total Work Time in CW') + " " + str(current_year) + "-" + str(current_cw)

    worked_amount = []

    for i in range(1, 8):
        datetime_i = dt.datetime.strptime(str(current_year) + "_" + str(current_cw) + "_" + str(i), "%G_%V_%u")
        date_sum = Interval.objects.filter(
            user=user,
            start__date=datetime_i.date(),
        )
        if company:
            date_sum = date_sum.filter(company=company)
        else:
            date_sum = date_sum.filter(company__isnull=True)
        date_sum = date_sum.aggregate(duration=Sum(F("end") - F("start")))['duration']
        if date_sum:
            worked_amount.append(date_sum.total_seconds() / (60 * 60))
        else:
            worked_amount.append(0)

    context["worked_amount"] = worked_amount

    times["weekly detail statistics"] = (timezone.now() - begin).total_seconds()

    logger.info("get_information_for_week_display", times=times)
    return context
