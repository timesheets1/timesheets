from django.apps import AppConfig


class StatisticModuleConfig(AppConfig):
    name = 'statistic_module'
