from io import BytesIO

import xlsxwriter
from django.db.models import F, Sum
from django.db.models.functions import Trunc

from statistic_module.helper import round_timedelta


def aggregated_statistic_category_subcategory_excel_export(intervals, company_name: str):
    detail_data = intervals \
        .values("user__username", "start__date", "category__name", "subcategory__name") \
        .annotate(username=F("user__username")) \
        .annotate(category_name=F("category__name")) \
        .annotate(subcategory_name=F("subcategory__name")) \
        .annotate(date=F("start__date")) \
        .annotate(duration=Sum(Trunc("end", "minute") - Trunc("start", "minute"))) \
        .values("username", "category_name", "subcategory_name", "date", "duration")

    output = BytesIO()
    workbook = xlsxwriter.Workbook(output, {'remove_timezone': True})
    # formatting
    date_format = workbook.add_format({'num_format': 'yyyy-mm-dd'})
    time_format = workbook.add_format({'num_format': 'HH:MM'})
    datetime_format = workbook.add_format({'num_format': 'dd.mm.yyyy HH:MM'})
    hours_format = workbook.add_format({'num_format': '0.00'})
    decimal_format = workbook.add_format({'num_format': '#,##0.00'})

    worksheet = workbook.add_worksheet(name=company_name[:31])
    # column widths
    worksheet.set_column('A:D', 20)

    header_titles = [
        "Date",
        "Category",
        "Subcategory",
        "Duration/h",
    ]
    worksheet.write_row(0, 0, header_titles)

    row = 1
    for d in detail_data:
        worksheet.write_datetime(row, 0, d["date"], date_format)
        worksheet.write(row, 1, d["category_name"])
        worksheet.write(row, 2, d["subcategory_name"])
        worksheet.write_number(row, 3, d["duration"].total_seconds() / 3600, decimal_format)

        row += 1

    workbook.close()
    xlsx_data = output.getvalue()

    return xlsx_data
