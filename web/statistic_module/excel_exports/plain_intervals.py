from io import BytesIO

import xlsxwriter
from django.utils import timezone

from timesheets.templatetags.nice_timedelta import decimal_hours


def plain_intervals_excel_export(intervals, human_readable_name: str):
    output = BytesIO()
    workbook = xlsxwriter.Workbook(output, {'remove_timezone': True})
    # formatting
    date_format = workbook.add_format({'num_format': 'dd.mm.yyyy'})
    time_format = workbook.add_format({'num_format': 'HH:MM'})
    datetime_format = workbook.add_format({'num_format': 'dd.mm.yyyy HH:MM'})
    hours_format = workbook.add_format({'num_format': '0.00'})
    decimal_format = workbook.add_format({'num_format': '#,##0.00'})

    worksheet = workbook.add_worksheet(name=human_readable_name[:31])
    # column widths
    worksheet.set_column('A:A', 5)
    worksheet.set_column('B:I', 20)

    header_titles = [
        "ID",
        "User",
        "Start",
        "End",
        "Duration",
        "Company",
        "Category",
        "Subcategory",
        "Notes"
    ]
    worksheet.write_row(0, 0, header_titles)

    row = 1
    for interval in intervals:
        worksheet.write(row, 0, interval.pk)
        worksheet.write(row, 1, interval.user.username)

        worksheet.write_datetime(row, 2, timezone.localtime(interval.start), datetime_format)
        worksheet.write_datetime(row, 3, timezone.localtime(interval.end), datetime_format)
        worksheet.write(row, 4, decimal_hours(interval.get_duration()), hours_format)

        company = "none"
        if interval.company:
            company = interval.company.name

        worksheet.write(row, 5, company)
        if interval.category:
            worksheet.write(row, 6, interval.category.name)
        if interval.subcategory:
            worksheet.write(row, 7, interval.subcategory.name)
        worksheet.write(row, 8, interval.notes)

        row += 1

    workbook.close()
    xlsx_data = output.getvalue()

    return xlsx_data
