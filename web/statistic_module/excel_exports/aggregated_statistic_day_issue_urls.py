from io import BytesIO

import xlsxwriter
from django.db.models import F, Sum
from django.db.models.functions import Trunc

from statistic_module.helper import round_timedelta


def aggregated_statistic_day_issue_url_excel_export(intervals, human_readable_name: str):
    '''
    Answers the question: On which issues have I worked in a certain timeframe? Arrgegate by Issue and Day.
    '''
    detail_data = intervals \
        .values("start__date", "category__name", "subcategory__name", "issue_url", "issue_reference", "notes") \
        .annotate(username=F("user__username")) \
        .annotate(category_name=F("category__name")) \
        .annotate(subcategory_name=F("subcategory__name")) \
        .annotate(date=F("start__date")) \
        .annotate(duration=Sum(Trunc("end", "minute") - Trunc("start", "minute"))) \
        .values("username", "category_name", "subcategory_name", "date", "duration", "issue_url", "issue_reference",
                "notes") \
        .order_by("date")

    output = BytesIO()
    workbook = xlsxwriter.Workbook(output, {'remove_timezone': True})
    # formatting
    date_format = workbook.add_format({'num_format': 'yyyy-mm-dd'})
    time_format = workbook.add_format({'num_format': 'HH:MM'})
    datetime_format = workbook.add_format({'num_format': 'dd.mm.yyyy HH:MM'})
    hours_format = workbook.add_format({'num_format': '0.00'})
    decimal_format = workbook.add_format({'num_format': '#,##0.00'})

    worksheet = workbook.add_worksheet(name=human_readable_name[:31])
    # column widths
    worksheet.set_column('A:A', 5)
    worksheet.set_column('B:I', 20)

    header_titles = [
        "User",
        "Issue URL",
        "Issue Reference",
        "Notes",
        "Category",
        "Subcategory",
        "Date",
        "Hours",
        "Duration",
        "Duration Rounded",
    ]
    worksheet.write_row(0, 0, header_titles)

    row = 1

    for d in detail_data:
        print(d)
        worksheet.write(row, 0, d["username"])
        worksheet.write(row, 1, d["issue_url"])
        worksheet.write(row, 2, d["issue_reference"])
        worksheet.write(row, 3, d["notes"])
        worksheet.write(row, 4, d["category_name"])
        worksheet.write(row, 5, d["subcategory_name"])
        worksheet.write(row, 6, d["date"], date_format)
        worksheet.write(row, 7, d["duration"].total_seconds() / 3600, decimal_format)
        worksheet.write_datetime(row, 8, d["duration"], time_format)
        worksheet.write_datetime(row, 9, round_timedelta(td=d["duration"], round_to=60 * 15), time_format)
        row += 1

    workbook.close()
    xlsx_data = output.getvalue()

    return xlsx_data
