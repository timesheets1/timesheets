# Generated by Django 3.1 on 2021-05-24 13:44

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('statistic_module', '0004_auto_20210510_2015'),
    ]

    operations = [
        migrations.CreateModel(
            name='TokenAccessLog',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('datetime', models.DateTimeField(auto_now_add=True)),
                ('request', models.JSONField(editable=False)),
                ('token', models.ForeignKey(editable=False, on_delete=django.db.models.deletion.CASCADE, to='statistic_module.statistictoken')),
            ],
        ),
    ]
