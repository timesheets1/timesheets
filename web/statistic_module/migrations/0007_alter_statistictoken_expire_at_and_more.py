# Generated by Django 4.1 on 2022-10-20 18:24

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
import uuid


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('statistic_module', '0006_alter_tokenaccesslog_id'),
    ]

    operations = [
        migrations.AlterField(
            model_name='statistictoken',
            name='expire_at',
            field=models.DateField(blank=True, null=True, verbose_name='Expire at'),
        ),
        migrations.AlterField(
            model_name='statistictoken',
            name='filter_data',
            field=models.JSONField(blank=True, help_text='Add defaults for forms that cannot be changed when using the access token.', null=True, verbose_name='Filter data'),
        ),
        migrations.AlterField(
            model_name='statistictoken',
            name='statistic',
            field=models.CharField(choices=[('INT', 'Intervals')], max_length=4, verbose_name='Statistic'),
        ),
        migrations.AlterField(
            model_name='statistictoken',
            name='token',
            field=models.UUIDField(default=uuid.uuid4, editable=False, primary_key=True, serialize=False, verbose_name='Token'),
        ),
        migrations.AlterField(
            model_name='statistictoken',
            name='user',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL, verbose_name='User'),
        ),
        migrations.AlterField(
            model_name='statistictoken',
            name='valid_from',
            field=models.DateField(blank=True, null=True, verbose_name='Valid from'),
        ),
        migrations.AlterField(
            model_name='tokenaccesslog',
            name='datetime',
            field=models.DateTimeField(auto_now_add=True, verbose_name='Datetime'),
        ),
        migrations.AlterField(
            model_name='tokenaccesslog',
            name='request',
            field=models.JSONField(editable=False, verbose_name='Request'),
        ),
        migrations.AlterField(
            model_name='tokenaccesslog',
            name='token',
            field=models.ForeignKey(editable=False, on_delete=django.db.models.deletion.CASCADE, to='statistic_module.statistictoken', verbose_name='Token'),
        ),
    ]
