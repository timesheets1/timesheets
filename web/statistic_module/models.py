import uuid

from allauth.utils import build_absolute_uri
from django.contrib.postgres.fields import JSONField
from django.urls import reverse
from django.utils.translation import gettext_lazy as _, gettext_lazy

from django.contrib.auth.models import User
from django.db import models


class ImplementedStatistics(models.TextChoices):
    INTERVALS = 'INT', _('Intervals')


class StatisticToken(models.Model):
    token = models.UUIDField(verbose_name=gettext_lazy("Token"), primary_key=True, editable=False, default=uuid.uuid4)
    user = models.ForeignKey(User, verbose_name=gettext_lazy("User"), on_delete=models.CASCADE)

    statistic = models.CharField(verbose_name=gettext_lazy("Statistic"), max_length=4,
                                 choices=ImplementedStatistics.choices)

    valid_from = models.DateField(verbose_name=gettext_lazy("Valid from"), null=True, blank=True)
    expire_at = models.DateField(verbose_name=gettext_lazy("Expire at"), null=True, blank=True)

    filter_data = models.JSONField(verbose_name=gettext_lazy("Filter data"), null=True, blank=True,
                                   help_text="Add defaults for forms that cannot be changed when using the access token.")

    def get_link(self, request=None):
        base_url = ''
        if self.statistic == ImplementedStatistics.INTERVALS:
            base_url = reverse('interval_overview_and_export')

        base_url += "/" + str(self.token)
        if request:
            base_url = build_absolute_uri(request, location=base_url)
        return base_url

    class Meta:
        verbose_name = gettext_lazy("Statistic Token")
        verbose_name_plural = gettext_lazy("Statistic Tokens")
        ordering = ["user", "valid_from"
                    ]


class TokenAccessLog(models.Model):
    datetime = models.DateTimeField(verbose_name=gettext_lazy("Datetime"), editable=False, auto_now_add=True)
    token = models.ForeignKey(StatisticToken, verbose_name=gettext_lazy("Token"), editable=False,
                              on_delete=models.CASCADE)
    request = models.JSONField(verbose_name=gettext_lazy("Request"), editable=False)

    class Meta:
        verbose_name = gettext_lazy("Token access log")
        verbose_name_plural = gettext_lazy("Token access logs")
        ordering = ["datetime"]
