from django.contrib import admin

# Register your models here.
from statistic_module.models import StatisticToken, TokenAccessLog


@admin.register(StatisticToken)
class StatisticTokenAdmin(admin.ModelAdmin):
    list_display = ["token", "statistic", "valid_from", "expire_at"]

@admin.register(TokenAccessLog)
class TokenAccessLogAdmin(admin.ModelAdmin):
    list_display = ["datetime", "token"]