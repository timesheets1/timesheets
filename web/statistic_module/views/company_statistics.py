from _decimal import Decimal
from io import BytesIO

import structlog
import xlsxwriter
from django.contrib.auth.decorators import login_required
from django.db.models import Sum, F, Value, CharField, Q
from django.db.models.functions import Round, Trunc, Concat
from django.http import HttpResponse
from django.shortcuts import get_object_or_404
from django.template import loader
from django.utils.decorators import method_decorator
from django.views import View

from management.models import Company
from statistic_module.excel_exports.aggregated_statistic_cat_subcat import \
    aggregated_statistic_category_subcategory_excel_export
from statistic_module.excel_exports.aggregated_statistic_day_issue_urls import \
    aggregated_statistic_day_issue_url_excel_export
from statistic_module.excel_exports.aggregated_statistic_day_notes import aggregated_statistic_day_notes_excel_export
from statistic_module.forms.interval_filter import IntervalFilterCompanyStatisticForm
from statistic_module.helper import get_hourly_rate
from timesheets.models import Interval, Category, Subcategory
from timesheets.templatetags.nice_timedelta import decimal_hours
from timesheets.utils import daterange


@method_decorator(login_required, name='dispatch')
class CompanyIntervalStatistics(View):

    def get(self, request, company_id):
        '''
        calculate aggregate statistics based on filter
        category, subcategory, start_date, end_date
        '''
        template = loader.get_template('statistic_module/company_interval_statistics.html')
        context = {}

        company = get_object_or_404(
            Company,
            id=company_id
        )
        context["company"] = company

        form = IntervalFilterCompanyStatisticForm(data=request.GET, company=company)
        context["form"] = form

        return HttpResponse(template.render(context, request))

    def post(self, request, company_id):
        template = loader.get_template('statistic_module/company_interval_statistics.html')
        context = {}
        logger = structlog.getLogger("timesheets")
        company = get_object_or_404(
            Company,
            id=company_id
        )
        context["company"] = company

        form = IntervalFilterCompanyStatisticForm(data=request.POST, company=company)

        if form.is_valid():
            context["form"] = form
            filter_start = None
            filter_end = None
            filter_categories = None
            filter_subcategories = None
            filter_users = None
            filter_billable = None
            logger.info("CompanyIntervalStatistics", filter=form.cleaned_data)
            if "start" in form.cleaned_data and form.cleaned_data["start"]:
                filter_start = form.cleaned_data["start"]

            if "end" in form.cleaned_data and form.cleaned_data["end"]:
                filter_end = form.cleaned_data["end"]

            if "categories" in form.cleaned_data and form.cleaned_data["categories"]:
                filter_categories = form.cleaned_data["categories"]

            if "subcategories" in form.cleaned_data and form.cleaned_data["subcategories"]:
                filter_subcategories = form.cleaned_data["subcategories"]

            if "users" in form.cleaned_data and form.cleaned_data["users"]:
                filter_users = form.cleaned_data["users"]

            if "billable" in form.cleaned_data:
                filter_billable = form.cleaned_data["billable"]

            # relevant categories
            categories = Category.objects.filter(
                company=company,
                working_time=True,
            )
            if filter_categories:
                categories = categories.filter(
                    id__in=[c.pk for c in filter_categories]
                )

            # relevant subcategories
            subcategories = Subcategory.objects.filter(
                company=company
            )
            if filter_subcategories:
                subcategories = subcategories.filter(
                    id__in=[sc.pk for sc in filter_subcategories]
                )

            # relevant intervals
            intervals = Interval.objects.filter(
                category__in=categories,
                subcategory__in=subcategories
            )

            if filter_billable is not None:
                # if filter_billable = True, we filter for hourly_rate__isnull=False,
                # so we only look at non-null hourly rates thus filtering billable subcategories
                intervals = intervals.filter(
                    Q(category__hourly_rate__isnull=not filter_billable) |
                    Q(subcategory__hourly_rate__isnull=not filter_billable)

                )

            if filter_users:
                intervals = intervals.filter(
                    user__in=filter_users
                )

            if filter_start:
                intervals = intervals.filter(
                    start__date__gte=filter_start
                )
            if filter_end:
                intervals = intervals.filter(
                    start__date__lte=filter_end
                )

            category_statistics = []
            subcategory_statistics = []
            day_statistics = []
            financial_statistics = {}

            sum_amount = 0
            sum_price = Decimal(0)

            # category statistics
            for category in categories:
                cat_intervals = intervals.filter(
                    category=category
                )
                category_amount = 0
                category_price = Decimal(0)

                # subcategory statistics
                for subcategory in subcategories:
                    subcat_intervals = intervals.filter(
                        category=category,
                        subcategory=subcategory
                    )
                    subcategory_amount = 0
                    timedelta = subcat_intervals.aggregate(duration=Sum(F("end") - F("start")))["duration"]
                    if timedelta:
                        subcategory_amount = decimal_hours(timedelta)
                    category_amount += subcategory_amount
                    if subcategory_amount:
                        hourly_rate = get_hourly_rate(category, subcategory)
                        subcategory_price = Decimal(0)
                        if hourly_rate:
                            subcategory_price = Decimal(subcategory_amount) * hourly_rate
                            category_price += subcategory_price

                        subcategory_statistics.append(
                            {
                                "category": category,
                                "subcategory": subcategory,
                                "amount": subcategory_amount,
                                "price": subcategory_price
                            }
                        )

                timedelta = cat_intervals.aggregate(duration=Sum(F("end") - F("start")))["duration"]
                if timedelta:
                    amount = decimal_hours(timedelta)
                sum_amount += category_amount
                sum_price += category_price
                # category statistics
                if category_amount:
                    # category statistics
                    category_statistics.append(
                        {
                            "category": category,
                            "amount": category_amount,
                            "price": category_price,
                        }
                    )
            financial_statistics["sum_price"] = sum_price
            financial_statistics["sum_amount"] = sum_amount

            context["financial_statistics"] = financial_statistics

            detail_data = intervals \
                .values("user__username", "start__date", "category__name", "subcategory__name") \
                .annotate(username=F("user__username")) \
                .annotate(category_name=F("category__name")) \
                .annotate(subcategory_name=F("subcategory__name")) \
                .annotate(date=F("start__date")) \
                .annotate(duration=Sum(Trunc("end", "minute") - Trunc("start", "minute"))) \
                .values("username", "category_name", "subcategory_name", "date", "duration")

            context["detail_data"] = detail_data
            human_readable_name = f"{company.name}"
            if filter_start and filter_end:
                human_readable_name += f"_{filter_start.strftime('%Y-%m-%d')}-{filter_end.strftime('%Y-%m-%d')}"

            if request.POST['action'] == 'download_agg_cat_subcat':
                xlsx_data = aggregated_statistic_category_subcategory_excel_export(intervals, company.name)

                response = HttpResponse(
                    xlsx_data,
                    content_type='application/vnd.ms-excel'
                )
                response['Content-Disposition'] = 'attachment; filename=' + human_readable_name + '.xlsx'
                return response
            if request.POST['action'] == 'download_agg_day_issue_url':
                xlsx_data = aggregated_statistic_day_issue_url_excel_export(intervals, company.name)

                response = HttpResponse(
                    xlsx_data,
                    content_type='application/vnd.ms-excel'
                )
                response['Content-Disposition'] = 'attachment; filename=' + human_readable_name + '.xlsx'
                return response
            if request.POST['action'] == 'download_agg_day_notes':

                xlsx_data = aggregated_statistic_day_notes_excel_export(intervals, human_readable_name)

                response = HttpResponse(
                    xlsx_data,
                    content_type='application/vnd.ms-excel'
                )
                response['Content-Disposition'] = 'attachment; filename=' + human_readable_name + '.xlsx'
                return response
            else:
                category_statistics = sorted(category_statistics, key=lambda k: k.get('amount', 0), reverse=True)
                subcategory_statistics = sorted(subcategory_statistics, key=lambda k: k.get('amount', 0), reverse=True)

                context["category_statistics"] = category_statistics
                context["subcategory_statistics"] = subcategory_statistics
                context["day_statistics"] = day_statistics
                return HttpResponse(template.render(context, request))

        else:
            # form is not valid
            context["form"] = form
            return HttpResponse(template.render(context, request))
