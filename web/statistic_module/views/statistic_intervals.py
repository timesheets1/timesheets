import datetime

from django.contrib.auth.decorators import login_required
from django.db.models import Sum, F, Q
from django.db.models.functions import Trunc
from django.http import HttpResponse, Http404
from django.utils import timezone
from io import BytesIO

import xlsxwriter
# Create your views here.
from django.template import loader

from management.models import Profile
from statistic_module.excel_exports.aggregated_statistic_day_issue_urls import \
    aggregated_statistic_day_issue_url_excel_export
from statistic_module.excel_exports.aggregated_statistic_day_notes import aggregated_statistic_day_notes_excel_export
from statistic_module.excel_exports.plain_intervals import plain_intervals_excel_export
from statistic_module.forms.interval_filter import IntervalFilterForm
from statistic_module.helper import log_token_access
from statistic_module.models import StatisticToken, ImplementedStatistics
from timesheets.helper import get_employments_for_user
from timesheets.models import Interval, Category, Subcategory


@login_required(login_url='account_login')
def interval_overview_and_export(request, token=None):
    template = loader.get_template('statistic_module/interval_overview_and_export.html')
    context = {}
    statistic_token = None

    if not token:
        user = request.user
        profile = Profile.objects.filter(user=user).first()

    else:
        statistic_token = StatisticToken.objects.filter(
            token=token,
            statistic=ImplementedStatistics.INTERVALS,
        )
        statistic_token = statistic_token.filter(
            Q(expire_at__isnull=True) |
            Q(expire_at__gte=timezone.now().date())
        )
        # check valid from
        statistic_token = statistic_token.filter(
            Q(valid_from__isnull=True) |
            Q(valid_from__lte=timezone.now().date()),
        )
        # check
        statistic_token = statistic_token.filter(
            Q(valid_from__isnull=True) |
            Q(valid_from__lte=timezone.now().date()),
        )

        statistic_token = statistic_token.first()
        if statistic_token:
            log_token_access(request=request, token=statistic_token)
            user = statistic_token.user

            context["token"] = statistic_token
            profile = Profile.objects.filter(user=user).first()
        else:
            raise Http404()

    human_readable_name = "Intervals " + user.username
    if request.method == "POST":
        form = IntervalFilterForm(request.POST, profile=profile, token=statistic_token)

        context["form"] = form
        if form.is_valid():

            employments = get_employments_for_user(user=user)

            intervals = Interval.objects.filter(
                user=user
            )

            categories = Category.objects.filter(
                company__employment__in=employments,
            )

            subcategories = Subcategory.objects.filter(
                company__employment__in=employments,
            )

            ## parse form
            start_date = None
            end_date = None
            filter_categories = None
            filter_subcategories = None

            if form.cleaned_data["start"]:
                start_date = form.cleaned_data["start"]
                context["start"] = start_date

            if form.cleaned_data["end"]:
                end_date = form.cleaned_data["end"]
                context["end"] = end_date

            if form.cleaned_data["categories"]:
                filter_categories = form.cleaned_data["categories"]

            if form.cleaned_data["subcategories"]:
                filter_subcategories = form.cleaned_data["subcategories"]

            ## apply filters
            if start_date:
                intervals = intervals.filter(
                    start__date__gte=start_date
                )

            if end_date:
                intervals = intervals.filter(
                    end__date__lte=end_date
                )

            if filter_categories:
                categories = categories.filter(
                    id__in=[c.pk for c in filter_categories]
                )

            if filter_subcategories:
                subcategories = subcategories.filter(
                    id__in=[sc.pk for sc in filter_subcategories]
                )

            ## parse token if applicable, only further constraints
            if statistic_token and statistic_token.filter_data:
                if "start" in statistic_token.filter_data:
                    try:
                        token_start_date = datetime.datetime.strptime(statistic_token.filter_data["start"], "%Y-%m-%d")
                        intervals = intervals.filter(
                            start_date__gte=token_start_date
                        )
                    except:
                        pass
                if "end" in statistic_token.filter_data:
                    try:
                        token_end_date = datetime.datetime.strptime(statistic_token.filter_data["end"], "%Y-%m-%d")
                        intervals = intervals.fiter(
                            end_date__lte=token_end_date
                        )
                    except:
                        pass
                if "categories" in statistic_token.filter_data:
                    categories = categories.filter(
                        name__in=statistic_token.filter_data["categories"]
                    )

                if "subcategories" in statistic_token.filter_data:
                    subcategories = subcategories.filter(
                        name__in=statistic_token.filter_data["subcategories"]
                    )

            intervals = intervals.filter(
                Q(subcategory__in=subcategories) |
                Q(subcategory__isnull=True),
                category__in=categories,
            )

            ## statistics

            category_statistics = []
            for category in categories:
                data = intervals.filter(category=category)
                amount = data.aggregate(duration=Sum(F("end") - F("start")))["duration"]
                duration = datetime.timedelta()
                if not amount:
                    amount = duration
                category_statistics.append({
                    "category": category,
                    "amount": amount,
                })
            category_statistics = sorted(category_statistics, key=lambda k: k.get('amount', 0), reverse=True)
            context["category_statistics"] = category_statistics

            intervals_subcategory = intervals.filter(
                category__in=categories,
                subcategory__in=subcategories,
            )
            if intervals_subcategory:
                subcategory_statistics = []
                for subcategory in subcategories:
                    data = intervals_subcategory.filter(subcategory=subcategory)
                    amount = data.aggregate(duration=Sum(F("end") - F("start")))["duration"]
                    duration = datetime.timedelta()
                    if not amount:
                        amount = duration
                    subcategory_statistics.append({
                        "subcategory": subcategory,
                        "amount": amount,
                    })
                subcategory_statistics = sorted(subcategory_statistics, key=lambda k: k.get('amount', 0), reverse=True)
                context["subcategory_statistics"] = subcategory_statistics

            context["interval_list"] = intervals.order_by(
                "start"
            )

            if start_date and end_date:
                human_readable_name += f"_{start_date.strftime('%y-%m-%d')}-{end_date.strftime('%y-%m-%d')}"

            if request.POST['action'] == 'download':
                xlsx_data = plain_intervals_excel_export(intervals, human_readable_name)
                response = HttpResponse(
                    xlsx_data,
                    content_type='application/vnd.ms-excel'
                )
                response['Content-Disposition'] = 'attachment; filename=' + human_readable_name + '.xlsx'
                return response

            if request.POST['action'] == 'download_aggregated':
                xlsx_data = aggregated_statistic_day_notes_excel_export(intervals, human_readable_name)
                response = HttpResponse(
                    xlsx_data,
                    content_type='application/vnd.ms-excel'
                )
                response['Content-Disposition'] = 'attachment; filename=' + human_readable_name + '_aggregated.xlsx'
                return response
            if request.POST['action'] == 'download_agg_day_issue_url':
                xlsx_data = aggregated_statistic_day_issue_url_excel_export(intervals, human_readable_name)
                response = HttpResponse(
                    xlsx_data,
                    content_type='application/vnd.ms-excel'
                )
                response['Content-Disposition'] = 'attachment; filename=' + human_readable_name + '_aggregated.xlsx'
                return response


    else:
        form = IntervalFilterForm(profile=profile, token=statistic_token)
        context["form"] = form

    return HttpResponse(template.render(context, request))
