from django.contrib.auth.decorators import login_required
from django.http import HttpResponse
from django.shortcuts import get_object_or_404, redirect
from django.template import loader

from statistic_module.forms.statistic_token import StatisticTokenForm
from statistic_module.models import StatisticToken, TokenAccessLog


@login_required(login_url='account_login')
def tokens_list_create(request):
    template = loader.get_template('statistic_module/statistic_tokens_list_create.html')
    context = {}

    if request.method == "POST":

        form = StatisticTokenForm(request.POST)
        context["form"] = form
        if form.is_valid():
            statistic_token = StatisticToken(
                user=request.user,
                statistic=form.cleaned_data["statistic"],
                valid_from=form.cleaned_data["valid_from"],
                expire_at=form.cleaned_data["expire_at"],
                filter_data=form.cleaned_data["filter_data"],
            )
            statistic_token.save()
            form = StatisticTokenForm()
    else:
        form = StatisticTokenForm()
        context["form"] = form

    tokens = StatisticToken.objects.filter(user=request.user)
    token_list = []
    for token in tokens:
        data = {
            "token": token,
            "accesses": TokenAccessLog.objects.filter(
                token=token,
            ).count(),
            "url": token.get_link(request=request)
        }
        token_list.append(data)

    context["token_list"] = token_list



    return HttpResponse(template.render(context, request))


@login_required(login_url='account_login')
def tokens_update_delete(request, token_id):
    template = loader.get_template('statistic_module/statistic_tokens_update_delete.html')
    context = {}

    token = get_object_or_404(
        StatisticToken,
        token=token_id,
        user=request.user,
    )

    if request.method == "POST":
        form = StatisticTokenForm(request.POST)
        if 'delete' in request.POST:
            token.delete()
            return redirect("statistic_tokens_list_create")

        if form.is_valid():
            token.statistic = form.cleaned_data["statistic"]
            token.valid_from = form.cleaned_data["valid_from"]
            token.expire_at = form.cleaned_data["expire_at"]
            token.filter_data = form.cleaned_data["filter_data"]
            token.save()
            return redirect("statistic_tokens_list_create")
    else:
        form = StatisticTokenForm(initial={
            "statistic": token.statistic,
            "valid_from": token.valid_from,
            "expire_at": token.expire_at,
            "filter_data": token.filter_data
        })

    context["form"] = form


    return HttpResponse(template.render(context, request))


@login_required(login_url='account_login')
def token_detail_view(request, token_id):
    template = loader.get_template('statistic_module/token_detail.html')
    context = {}
    token = get_object_or_404(
        StatisticToken,
        token=token_id,
        user=request.user,
    )
    context["token"] = token

    access_logs = TokenAccessLog.objects.filter(token=token).order_by("-datetime")
    context["access_logs"] = access_logs

    return HttpResponse(template.render(context, request))
