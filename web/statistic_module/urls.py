from django.urls import path

from statistic_module.views.company_statistics import CompanyIntervalStatistics
from statistic_module.views.statistic_intervals import interval_overview_and_export
from statistic_module.views.statistic_tokens import tokens_list_create, tokens_update_delete, token_detail_view

urlpatterns = [
	path('tokens', tokens_list_create, name="statistic_tokens_list_create"),
	path('tokens/<token_id>', tokens_update_delete, name="statistic_tokens_update_delete"),
	path('tokens/<token_id>/details', token_detail_view, name="statistic_token_detail_view"),
	path('interval_overview_and_export', interval_overview_and_export, name='interval_overview_and_export'),
	path('interval_overview_and_export/<token>', interval_overview_and_export, name='interval_overview_and_export_token'),
	path('company/<int:company_id>/interval_statistics', CompanyIntervalStatistics.as_view(), name="company_interval_statistics")
]