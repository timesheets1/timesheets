import datetime

from statistic_module.models import TokenAccessLog
from timesheets.models import Category, Subcategory


def log_token_access(request, token):
    request_dict = {
        "scheme": request.scheme,
        "body": str(request.body),
        "path": request.path,
        "path_info": request.path_info,
        "method": request.method,
        "encoding": request.encoding,
        "content_type": request.content_type,
        "content_params": request.content_params,
        "get": request.GET,
        "post": request.POST,
        "cookies": request.COOKIES,
        "files": request.FILES,
    }
    if "HTTP_USER_AGENT" in request.META:
        request_dict["user_agent"] = request.META["HTTP_USER_AGENT"]
    if "REMOTE_ADDR" in request.META:
        request_dict["client_ip"] = request.META["REMOTE_ADDR"]
    if "REMOTE_HOST" in request.META:
        request_dict["client_hostname"] = request.META["REMOTE_HOST"]
    if "HTTP_HOST" in request.META:
        request_dict["http_host"] = request.META["HTTP_HOST"]

    log = TokenAccessLog(
        request=request_dict,
        token=token
    )
    log.save()


def round_timedelta(td: datetime.timedelta, round_to: int):
    """Round a timedelta object to any time lapse in seconds
    """
    seconds = td.total_seconds()
    return datetime.timedelta(seconds=round(seconds / round_to) * round_to)


def get_hourly_rate(category: Category, subcategory: Subcategory):
    hourly_rate = None
    if category.hourly_rate is not None:
        hourly_rate = category.hourly_rate
    if subcategory.hourly_rate is not None:
        hourly_rate = subcategory.hourly_rate
    return hourly_rate
