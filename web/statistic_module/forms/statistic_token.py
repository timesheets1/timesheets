from django import forms
from django.utils.translation import gettext_lazy

from statistic_module.models import ImplementedStatistics
from webTimesheets.custom_form_fields import DateInput


class StatisticTokenForm(forms.Form):
    statistic = forms.ChoiceField(
        label=gettext_lazy("Statistic"),
        choices=ImplementedStatistics.choices,
    )
    valid_from = forms.DateField(
        label=gettext_lazy("Valid from"),
        widget=DateInput(),
        required=False,
        input_formats=["%Y-%m-%d", "%d.%m.%Y"],
        help_text="2020-01-31 or 31.01.2020"
    )
    expire_at = forms.DateField(
        label=gettext_lazy("Expire at"),
        widget=DateInput(),
        required=False,
        input_formats=["%Y-%m-%d", "%d.%m.%Y"],
        help_text="2020-01-31 or 31.01.2020"
    )
    filter_data = forms.JSONField(
        label=gettext_lazy("Filter Data"),
        required=False,
    )