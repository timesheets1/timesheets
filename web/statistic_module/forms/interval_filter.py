from django import forms
from django.contrib.auth.models import User
from django.core.exceptions import ValidationError
from django.db.models import Q, F
from django.utils.translation import gettext_lazy

from management.models import Employment, Profile
from timesheets.helper import get_companies_for_user
from timesheets.models import Category, Subcategory
from webTimesheets.custom_form_fields import DateInput


class IntervalFilterCompanyStatisticForm(forms.Form):
    start = forms.DateField(
        label=gettext_lazy("Start"),
        widget=DateInput(),
        required=False,
    )
    end = forms.DateField(
        label=gettext_lazy("End"),
        widget=DateInput(),
        required=False,
    )

    categories = forms.ModelMultipleChoiceField(
        label=gettext_lazy("Categories"),
        required=False,
        widget=forms.SelectMultiple(attrs={'class': 'selectCategories'}),
        queryset=Category.objects.none()
    )

    subcategories = forms.ModelMultipleChoiceField(
        label=gettext_lazy("Subcategories"),
        required=False,
        widget=forms.SelectMultiple(attrs={'class': 'selectSubcategories'}),
        queryset=Subcategory.objects.none()
    )
    billable = forms.NullBooleanField(
        label=gettext_lazy("Billable"),
        required=False,
    )
    users = forms.ModelMultipleChoiceField(
        label=gettext_lazy("Users"),
        required=False,
        widget=forms.SelectMultiple(attrs={'class': 'selectUsers'}),
        queryset=User.objects.none()
    )

    def __init__(self, *args, **kwargs):
        self.company = kwargs.pop('company', None)

        super().__init__(*args, **kwargs)

        categories = Category.objects.filter(
            company=self.company,
        )
        subcategories = Subcategory.objects.filter(
            company=self.company,
        )

        employments = Employment.objects.filter(
            company=self.company
        )
        profiles = Profile.objects.filter(
            employment__in=employments
        )

        self.fields['categories'].queryset = categories
        self.fields['subcategories'].queryset = subcategories
        self.fields['users'].queryset = User.objects.filter(
            id__in=profiles.values_list("user_id", flat=True)
        )


class IntervalFilterForm(forms.Form):
    start = forms.DateField(
        label=gettext_lazy("Start"),
        widget=DateInput(),
        required=False,
        input_formats=["%Y-%m-%d", "%d.%m.%Y"],
        help_text="2020-01-31 or 31.01.2020"
    )
    end = forms.DateField(
        label=gettext_lazy("End"),
        widget=DateInput(),
        required=False,
        input_formats=["%Y-%m-%d", "%d.%m.%Y"],
        help_text="2020-01-31 or 31.01.2020"
    )

    categories = forms.ModelMultipleChoiceField(
        label=gettext_lazy("Categories"),
        required=False,
        widget=forms.SelectMultiple(attrs={'class': 'selectCategories'}),
        queryset=Category.objects.none()
    )

    subcategories = forms.ModelMultipleChoiceField(
        label=gettext_lazy("Subcategories"),
        required=False,
        widget=forms.SelectMultiple(attrs={'class': 'selectSubcategories'}),
        queryset=Subcategory.objects.none()
    )

    def __init__(self, *args, **kwargs):
        self.profile = kwargs.pop('profile', None)
        self.token = kwargs.pop('token', None)
        super().__init__(*args, **kwargs)

        companies = get_companies_for_user(self.profile.user)

        categories = Category.objects.all()
        subcategories = Subcategory.objects.all()

        # overwrite queryset
        if self.profile:
            if companies:
                categories = categories.filter(
                    Q(company__isnull=True) | Q(company__in=companies)
                )
                subcategories = subcategories.filter(
                    Q(company__isnull=True) | Q(company__in=companies)
                )

        if self.token and self.token.filter_data:
            for key, value in self.token.filter_data.items():
                if key in self.fields:
                    if key == "categories":
                        categories = categories.filter(
                            name__in=value,
                        )
                    elif key == "subcategories":
                        subcategories = subcategories.filter(
                            name__in=value,
                        )
                    else:
                        self.fields[key].initial = value
                        self.fields[key].disabled = True
        categories = categories.order_by("-company").order_by(F('company').desc(nulls_last=True))
        subcategories = subcategories.order_by("-company").order_by(F('company').desc(nulls_last=True))

        self.fields['categories'].queryset = categories
        self.fields['subcategories'].queryset = subcategories
