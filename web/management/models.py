import uuid
from decimal import Decimal

from django.contrib.auth.models import User
from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.utils.translation import gettext_lazy


class Company(models.Model):
	name = models.CharField(verbose_name=gettext_lazy("Name"), max_length=99, unique=True)

	def __str__(self):
		return self.name

	class Meta:
		verbose_name = gettext_lazy("Company")
		verbose_name_plural = gettext_lazy("Companies")


class PublicHoliday(models.Model):
	id = models.UUIDField(verbose_name=gettext_lazy("Id"), primary_key=True, default=uuid.uuid4, editable=False)
	name = models.CharField(verbose_name=gettext_lazy("Name"), max_length=99)
	date = models.DateField(verbose_name=gettext_lazy("Date"), )
	weight = models.DecimalField(verbose_name=gettext_lazy("Weight"), max_digits=2, decimal_places=1, default=Decimal(1.0))
	company = models.ForeignKey(Company, verbose_name=gettext_lazy("Company"), on_delete=models.CASCADE)

	def __str__(self):
		return self.name

	class Meta:
		unique_together = ["date", "company"]
		verbose_name = gettext_lazy("Public Holiday")
		verbose_name_plural = gettext_lazy("Public Holidays")
		ordering = ["company", "-date"]


class Profile(models.Model):
	user = models.OneToOneField(User, verbose_name=gettext_lazy("User"), on_delete=models.CASCADE)

	def __str__(self):
		return self.user.__str__()


@receiver(post_save, sender=User)
def create_user_profile(sender, instance, created, **kwargs):
	if created:
		Profile.objects.create(user=instance)


@receiver(post_save, sender=User)
def save_user_profile(sender, instance, **kwargs):
	instance.profile.save()


class Employment(models.Model):
	profile = models.ForeignKey(Profile, verbose_name=gettext_lazy("Profile"), on_delete=models.CASCADE)
	company = models.ForeignKey(Company, verbose_name=gettext_lazy("Company"), on_delete=models.CASCADE)
	start = models.DateField(verbose_name=gettext_lazy("Start"))
	end = models.DateField(verbose_name=gettext_lazy("End"), blank=True, null=True)
	is_admin_for_company = models.BooleanField(verbose_name=gettext_lazy("Is admin for company"), default=False)
	order = models.PositiveIntegerField(verbose_name=gettext_lazy("Order"), default=1)

	offset_overtime_hour_account = models.DecimalField(verbose_name=gettext_lazy("Offset overtime hour account"), max_digits=7, decimal_places=2, default=0, help_text="Overtime balance since the beginning of the employment when starting to use Timesheets.")
	offset_holiday_days = models.DecimalField(verbose_name=gettext_lazy("Offset holiday days"), max_digits=7, decimal_places=2, default=0, help_text="Total sum of taken holiday days since the beginning of the employment when starting to use Timesheets.")
	usage_start = models.DateField(verbose_name=gettext_lazy("Usage start"), blank=True, null=True, help_text="If set, use this instead of start in order to calculate overtime and holidays")
	class Meta:
		unique_together = ["profile", "company"]
		verbose_name = gettext_lazy("Employment")
		verbose_name_plural = gettext_lazy("Employments")

	def __str__(self):
		return self.company.name + " - " + self.profile.__str__()



class ContractPeriod(models.Model):
	start = models.DateField(verbose_name=gettext_lazy("Start"), )
	end = models.DateField(verbose_name=gettext_lazy("End"), blank=True, null=True)
	work_hours_amount = models.DecimalField(verbose_name=gettext_lazy("Work hours amount"), max_digits=4, decimal_places=2)
	holiday_days_amount = models.DecimalField(verbose_name=gettext_lazy("Holiday days amount"), max_digits=4, decimal_places=2)
	employment = models.ForeignKey(Employment, verbose_name=gettext_lazy("Employment"), blank=True, null=True, on_delete=models.CASCADE)

	monday = models.BooleanField(verbose_name=gettext_lazy("Monday"), default=True)
	tuesday = models.BooleanField(verbose_name=gettext_lazy("Tuesday"), default=True)
	wednesday = models.BooleanField(verbose_name=gettext_lazy("Wednesday"), default=True)
	thursday = models.BooleanField(verbose_name=gettext_lazy("Thursday"), default=True)
	friday = models.BooleanField(verbose_name=gettext_lazy("Friday"), default=True)
	saturday = models.BooleanField(verbose_name=gettext_lazy("Saturday"), default=False)
	sunday = models.BooleanField(verbose_name=gettext_lazy("Sunday"), default=False)

	def get_working_days_per_week(self):
		days = 0
		if self.monday:
			days += 1
		if self.tuesday:
			days += 1
		if self.wednesday:
			days += 1
		if self.thursday:
			days += 1
		if self.friday:
			days += 1
		if self.saturday:
			days += 1
		if self.sunday:
			days += 1
		return days

	def get_working_days(self):
		days = []
		if self.monday:
			days.append("Mo")
		if self.tuesday:
			days.append("Tue")
		if self.wednesday:
			days.append("Wed")
		if self.thursday:
			days.append("Th")
		if self.friday:
			days.append("Fri")
		if self.saturday:
			days.append("Sa")
		if self.sunday:
			days.append("Su")
		return days


	class Meta:
		verbose_name = gettext_lazy("Contract period")
		verbose_name_plural = gettext_lazy("Contract periods")
		ordering = ["employment__company", "start"]