from django import forms
from django.contrib.auth.models import User
from django.core.exceptions import ValidationError
from django.db.models import F, Q
from django.utils.translation import gettext_lazy

from management.models import ContractPeriod, Employment, PublicHoliday
from timesheets.helper import get_companies_for_user
from timesheets.models import Category, Subcategory
from webTimesheets.custom_form_fields import DateInput


class ContractPeriodForm(forms.Form):
    start = forms.DateField(
        label=gettext_lazy("Start"),
        widget=DateInput(),
        required=True,
        input_formats=["%Y-%m-%d", "%d.%m.%Y"],
        help_text="2020-01-31 or 31.01.2020"
    )
    end = forms.DateField(
        label=gettext_lazy("End"),
        widget=DateInput(),
        required=False,
        input_formats=["%Y-%m-%d", "%d.%m.%Y"],
        help_text="2020-01-31 or 31.01.2020"
    )
    work_hours_amount = forms.FloatField(label=gettext_lazy("Work Hours Amount"), required=True)
    holiday_days_amount = forms.FloatField(label=gettext_lazy("Holiday Days Amount"), required=True)
    monday = forms.BooleanField(label=gettext_lazy("Monday"), required=False)
    tuesday = forms.BooleanField(label=gettext_lazy("Tuesday"), required=False)
    wednesday = forms.BooleanField(label=gettext_lazy("Wednesday"), required=False)
    thursday = forms.BooleanField(label=gettext_lazy("Thursday"), required=False)
    friday = forms.BooleanField(label=gettext_lazy("Friday"), required=False)
    saturday = forms.BooleanField(label=gettext_lazy("Saturday"), required=False)
    sunday = forms.BooleanField(label=gettext_lazy("Sunday"), required=False)


class DeleteContractPeriodForm(forms.ModelForm):
    class Meta:
        model = ContractPeriod
        fields = []


class EmploymentPersonalCreateForm(forms.Form):
    company = forms.CharField(
        label=gettext_lazy("Company"),
        required=True,
        help_text="Enter a company name to join it or create a new one if it does not exist yet."
    )
    start = forms.DateField(
        label=gettext_lazy("Start"),
        widget=DateInput(),
        required=True,
        input_formats=["%Y-%m-%d", "%d.%m.%Y"],
        help_text="2020-01-31 or 31.01.2020",
    )
    end = forms.DateField(
        label=gettext_lazy("End"),
        widget=DateInput(),
        required=False,
        input_formats=["%Y-%m-%d", "%d.%m.%Y"],
        help_text="2020-01-31 or 31.01.2020",
    )

    offset_overtime_hour_account = forms.DecimalField(
        label=gettext_lazy("Offset Overtime Hour Account"),
        max_digits=7,
        decimal_places=2,
        initial=0,
    )
    offset_holiday_days = forms.DecimalField(
        label=gettext_lazy("Offset Holiday Days"),
        max_digits=7,
        decimal_places=2,
        initial=0,
    )


class EmploymentAdminCreateForm(forms.Form):
    user = forms.ModelChoiceField(
        label=gettext_lazy("User"),
        queryset=User.objects.filter(is_active=True),
    )
    start = forms.DateField(
        label=gettext_lazy("Start"),
        widget=DateInput(),
        required=True,
        input_formats=["%Y-%m-%d", "%d.%m.%Y"],
        help_text="2020-01-31 or 31.01.2020",
    )
    end = forms.DateField(
        label=gettext_lazy("End"),
        widget=DateInput(),
        required=False,
        input_formats=["%Y-%m-%d", "%d.%m.%Y"],
        help_text="2020-01-31 or 31.01.2020")
    offset_overtime_hour_account = forms.DecimalField(
        label=gettext_lazy("Offset Overtime Hour Account"),
        max_digits=7,
        decimal_places=2,
        initial=0
    )
    offset_holiday_days = forms.DecimalField(
        label=gettext_lazy("Offset Holiday Days"),
        max_digits=7,
        decimal_places=2,
        initial=0,
    )
    is_admin_for_company = forms.BooleanField(
        label=gettext_lazy("Is Admin for Company"),
        required=False,
    )

    def __init__(self, *args, **kwargs):
        self.user_ids_already_employed_here = kwargs.pop('user_ids_already_employed_here', None)
        super().__init__(*args, **kwargs)

        # overwrite queryset
        if self.user_ids_already_employed_here:
            self.fields['user'].queryset = User.objects.exclude(
                id__in=self.user_ids_already_employed_here
            ).order_by("-username")

    def clean(self):
        cleaned_data = super().clean()
        start = cleaned_data.get("start")
        end = cleaned_data.get("end")

        if end:
            if end < start:
                raise ValidationError(
                    "End cannot be before Start."
                )


class EmploymentEditForm(forms.Form):
    start = forms.DateField(
        label=gettext_lazy("Start"),
        widget=DateInput(),
        required=True,
        input_formats=["%Y-%m-%d", "%d.%m.%Y"],
        help_text="2020-01-31 or 31.01.2020",
    )
    end = forms.DateField(
        label=gettext_lazy("End"),
        widget=DateInput(),
        required=False,
        input_formats=["%Y-%m-%d", "%d.%m.%Y"],
        help_text="2020-01-31 or 31.01.2020",
    )

    usage_start = forms.DateField(
        label=gettext_lazy("Usage Start"),
        widget=DateInput(),
        required=False,
        input_formats=["%Y-%m-%d", "%d.%m.%Y"],
        help_text="Start date of using timesheets, if not the same as start",
    )
    offset_overtime_hour_account = forms.DecimalField(
        label=gettext_lazy("Offset Overtime Hour Account"),
        max_digits=7,
        decimal_places=2,
        initial=0,
        help_text="How much overtime is accumulated when usage of timesheets starts?",
    )
    offset_holiday_days = forms.DecimalField(
        label=gettext_lazy("Offset Holiday Days"),
        max_digits=7,
        decimal_places=2,
        initial=0,
        help_text="How many holidays have already been taken before usage of timesheets that are not tracked individually?",
    )


class EmploymentAdminEditForm(forms.Form):
    start = forms.DateField(
        label=gettext_lazy("Start"),
        widget=DateInput(),
        required=True,
        input_formats=["%Y-%m-%d", "%d.%m.%Y"],
        help_text="2020-01-31 or 31.01.2020",
    )
    end = forms.DateField(
        label=gettext_lazy("End"),
        widget=DateInput(),
        required=False,
        input_formats=["%Y-%m-%d", "%d.%m.%Y"],
        help_text="2020-01-31 or 31.01.2020",
    )

    usage_start = forms.DateField(
        label=gettext_lazy("Usage Start"),
        widget=DateInput(),
        required=False,
        input_formats=["%Y-%m-%d", "%d.%m.%Y"],
        help_text="Start date of using timesheets, if not the same as start",
    )
    offset_overtime_hour_account = forms.DecimalField(
        label=gettext_lazy("Offset Overtime Hour Account"),
        max_digits=7,
        decimal_places=2,
        initial=0,
        help_text="How much overtime is accumulated when usage of timesheets starts?",
    )
    offset_holiday_days = forms.DecimalField(
        label=gettext_lazy("Offset Holiday Days"),
        max_digits=7,
        decimal_places=2,
        initial=0,
        help_text="How many holidays have already been taken before usage of timesheets that are not tracked individually?",
    )

    is_admin_for_company = forms.BooleanField(
        label=gettext_lazy("Is Admin for Company"),
        required=False,
        help_text="Grant admin privileges in this company",
    )


class EmploymentDeleteForm(forms.ModelForm):
    class Meta:
        model = Employment
        fields = []


class CategoryForm(forms.ModelForm):
    '''

    name = forms.CharField(
        label=gettext_lazy("Description"),
        required=True,
    )
    description = forms.CharField(
        label=gettext_lazy("Description"),
        required=False,
        widget=forms.Textarea(attrs={'rows': 5}),
    )
    working_time = forms.BooleanField(
        label=gettext_lazy("Working Time"),
        required=False,
        help_text="Overtime is only calculated for working time.",
    )
    bookable = forms.BooleanField(
        label=gettext_lazy("Bookable"),
        required=False,
        help_text="Decides, if this Category is available for booking in the Interval form.",
    )
    exclusive = forms.BooleanField(
        label=gettext_lazy("Exclusive"),
        required=False,
        help_text="Controls if intervals in this category are relevant for integrity check."
    )
    '''

    def __init__(self, *args, **kwargs):
        self.company = kwargs.pop('company', None)
        super().__init__(*args, **kwargs)

    def save(self, commit=True):
        instance = super().save(commit=False)
        instance.company = self.company

        if commit:
            instance.save()
            self.save_m2m()
        return instance

    class Meta:
        model = Category
        exclude = ["company"]


class CategoryDeleteForm(forms.ModelForm):
    class Meta:
        model = Category
        exclude = ["company"]


class SubcategoryForm(forms.ModelForm):
    '''

    name = forms.CharField(
        label=gettext_lazy("Name"),
        required=True,
    )
    description = forms.CharField(
        label=gettext_lazy("Description"),
        required=False,
        widget=forms.Textarea(attrs={'rows': 5}),
    )
    bookable = forms.BooleanField(
        label=gettext_lazy("Bookable"),
        required=False,
        help_text=gettext_lazy("Decides, if this Subcategory is available for booking in the Interval form."),
    )
    billalbe = forms.BooleanField(
        label=gettext_lazy("Billalble"),
        required=False,
        help_text=gettext_lazy("Indicates that work on this subcategory can be invoiced."),
    )
    '''

    def __init__(self, *args, **kwargs):
        self.company = kwargs.pop('company', None)
        super().__init__(*args, **kwargs)

    def save(self, commit=True):
        instance = super().save(commit=False)
        instance.company = self.company

        if commit:
            instance.save()
            self.save_m2m()
        return instance

    class Meta:
        model = Subcategory
        exclude = ["company"]


class SubcategoryDeleteForm(forms.ModelForm):
    class Meta:
        model = Category
        fields = []


class PublicHolidayForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        self.company = kwargs.pop('company', None)
        super().__init__(*args, **kwargs)

    def save(self, commit=True):
        instance = super().save(commit=False)
        instance.company = self.company

        if commit:
            instance.save()
            self.save_m2m()

        return instance

    class Meta:
        model = PublicHoliday
        exclude = ["company"]


class IntervalChangeCategoriesForm(forms.Form):
    old_category = forms.ModelChoiceField(
        label=gettext_lazy("Old Category"),
        queryset=Category.objects.filter(
            company__isnull=True,
        ),
        required=True,
    )

    old_subcategory = forms.ModelChoiceField(
        label=gettext_lazy("Old Subcategory"),
        queryset=Subcategory.objects.filter(
            company__isnull=True,
        ),
        required=True,
    )

    new_category = forms.ModelChoiceField(
        label=gettext_lazy("New Category"),
        queryset=Category.objects.filter(
            company__isnull=True,
        ).order_by(
            F('company').desc(nulls_last=True)
        ),
        required=True,
    )

    new_subcategory = forms.ModelChoiceField(
        label=gettext_lazy("New Subcategory"),
        queryset=Subcategory.objects.filter(
            company__isnull=True,
        ).order_by(
            F('company').desc(nulls_last=True)
        ),
        required=True,
    )

    def __init__(self, *args, **kwargs):
        self.profile = kwargs.pop('profile', None)
        super().__init__(*args, **kwargs)

        if self.profile:
            # overwrite queryset

            companies = get_companies_for_user(self.profile.user)
            if companies:
                available_categories = Category.objects.filter(
                    Q(company__isnull=True) |
                    Q(company__in=companies)
                )
                available_subcategories = Subcategory.objects.filter(
                    Q(company__isnull=True) |
                    Q(company__in=companies)
                )
                self.fields['old_category'].queryset = available_categories
                self.fields['old_subcategory'].queryset = available_subcategories
                self.fields['new_category'].queryset = available_categories
                self.fields['new_subcategory'].queryset = available_subcategories

    def clean(self):
        cleaned_data = super().clean()
        old_category = cleaned_data.get("old_category")
        old_subcategory = cleaned_data.get("old_subcategory")
        new_category = cleaned_data.get("new_category")
        new_subcategory = cleaned_data.get("new_subcategory")

        if old_category.company != old_subcategory.company or new_category.company != new_subcategory.company:
            raise ValidationError(gettext_lazy("Category and Subcategory must belong to the same company (or none)."))
