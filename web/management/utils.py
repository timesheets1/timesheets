import datetime
from decimal import Decimal

from django.db.models import Q, F, Sum

from holidays.models import HolidayDay
from management.models import ContractPeriod, Employment
from sickness.models.sickness_period import SicknessPeriod
from timesheets.models import Interval
from management.models import PublicHoliday


def contracted_work_time_at(employment: Employment, date: datetime.date):
    '''
    calculates the contracted work time at a certain date
    '''
    if employment:

        if employment.start > date:
            return 0

        if employment.usage_start and employment.usage_start > date:
            return 0

        sickness_periods = SicknessPeriod.objects.filter(
            start__lte=date,
            end__gte=date,
            employment=employment
        )

        if sickness_periods:
            # the employee was sick on this day
            return 0

        currentPeriod = ContractPeriod.objects.filter(
            employment=employment
        ).filter(
            Q(end__gte=date) | Q(end__isnull=True),
            start__lte=date
        ).first()

        work_time = 0

        if currentPeriod:
            if currentPeriod.monday and date.weekday() == 0:
                work_time = currentPeriod.work_hours_amount / currentPeriod.get_working_days_per_week()
            elif currentPeriod.tuesday and date.weekday() == 1:
                work_time = currentPeriod.work_hours_amount / currentPeriod.get_working_days_per_week()
            elif currentPeriod.wednesday and date.weekday() == 2:
                work_time = currentPeriod.work_hours_amount / currentPeriod.get_working_days_per_week()
            elif currentPeriod.thursday and date.weekday() == 3:
                work_time = currentPeriod.work_hours_amount / currentPeriod.get_working_days_per_week()
            elif currentPeriod.friday and date.weekday() == 4:
                work_time = currentPeriod.work_hours_amount / currentPeriod.get_working_days_per_week()
            elif currentPeriod.saturday and date.weekday() == 5:
                work_time = currentPeriod.work_hours_amount / currentPeriod.get_working_days_per_week()
            elif currentPeriod.sunday and date.weekday() == 6:
                work_time = currentPeriod.work_hours_amount / currentPeriod.get_working_days_per_week()

        public_holiday_name = PublicHoliday.objects.filter(date=date).first()

        if public_holiday_name:
            work_time = work_time * (Decimal(1.0) - public_holiday_name.weight)

        holiday_days = HolidayDay.objects.filter(
            date=date,
            employment=employment
        )
        if holiday_days:
            weight = holiday_days.aggregate(total_weight=Sum("weight"))["total_weight"]
            if weight > 1:
                weight = 1
            # for holidays, return the remaining amount
            work_time = work_time * (Decimal(1.0) - weight)

            if work_time < 0:
                work_time = 0

        return work_time

    else:
        return 0


def overlapping_intervals_integrity_check(user):
    issues_overlapping = []
    # overlapping intervals
    overlapping_intervals = Interval.objects.filter(
        user=user,
        category__exclusive=True,
    ).order_by('start')
    for i in range(len(overlapping_intervals) - 1):
        if overlapping_intervals[i].end > overlapping_intervals[i + 1].start:
            issues_overlapping.append(
                {'user': overlapping_intervals[i + 1].user, 'interval_a': overlapping_intervals[i],
                 'interval_b': overlapping_intervals[i + 1]})

    return issues_overlapping


def interval_integrity_check(user):
    issues_single_interval = []

    negative_duration_intervals = Interval.objects.filter(
        end__lt=F("start"),
    )
    for interval in negative_duration_intervals:
        issues_single_interval.append({
            "user": interval.user,
            "interval": interval,
            "error": "This interval ends before it starts"
        })
    return issues_single_interval


def employment_period_integrity_check(employment):
    contract_period_issues = []
    periods = ContractPeriod.objects.filter(
        employment=employment,
    ).order_by("start")
    periods = list(periods)
    if periods:
        for i in range(len(periods) - 1):
            if periods[i].end:
                if periods[i].end < periods[i].start:
                    contract_period_issues.append({
                        'company': periods[i].employment.company,
                        'period': periods[i],
                        'reason': "Period starts before it is over.",
                    })
                if periods[i + 1].start < periods[i].end:
                    contract_period_issues.append({
                        'company': periods[i + 1].employment.company,
                        'period': periods[i + 1],
                        'reason': "Period starts before the previous one is over.",
                    })
                if periods[i].end < periods[i].start:
                    contract_period_issues.append({
                        'company': periods[i + 1].employment.company,
                        'period': periods[i],
                        'reason': "Period ends before it starts.",
                    })
            else:
                # only the last interval may have an open end!
                contract_period_issues.append({
                    'company': periods[i].employment.company,
                    'period': periods[i],
                    'reason': "Only the last period may have an open end."
                })
        # check if last period has usable start and end
        if periods[-1].end:
            if periods[-1].end < periods[-1].start:
                contract_period_issues.append({
                    'company': periods[-1].employment.company,
                    'period': periods[-1],
                    'reason': "Last period ends before it starts.",
                })
    else:
        contract_period_issues.append({
            'reason': "No period is defined for this employment.",
        })

    return contract_period_issues
