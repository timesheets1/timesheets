from django.contrib.auth.decorators import login_required
from django.http import HttpResponse, Http404
from django.shortcuts import get_object_or_404
from django.template import loader

from management.models import Profile, Employment, PublicHoliday, Company
from management.utils import employment_period_integrity_check, overlapping_intervals_integrity_check, \
    interval_integrity_check
from timesheets.models import Category, Subcategory


@login_required(login_url='account_login')
def company_detail_view(request, company_pk):
    template = loader.get_template('company_detail.html')

    context = {}
    company = get_object_or_404(Company, id=company_pk)
    profile = Profile.objects.filter(user=request.user).first()

    employment = Employment.objects.filter(
        profile=profile,
        company=company,
    ).first()

    if employment and employment.is_admin_for_company:
        context["company"] = company

        employments_in_company = []
        employments_in_company_qs = Employment.objects.filter(company=company).order_by("start")
        for employment in employments_in_company_qs:
            employments_in_company.append({
                "employment": employment,
                "employment_issues": len(employment_period_integrity_check(employment=employment)),
                "interval_issues": len(overlapping_intervals_integrity_check(user=employment.profile.user)) + len(
                    interval_integrity_check(user=employment.profile.user)),
            })
        context["employments_in_company"] = employments_in_company

        categories = Category.objects.filter(company=company).order_by("name")
        context["categories"] = categories

        subcategories = Subcategory.objects.filter(company=company).order_by("name")
        context["subcategories"] = subcategories

        public_holidays = PublicHoliday.objects.filter(company=company)
        context["public_holidays"] = public_holidays

        return HttpResponse(template.render(context, request))

    else:
        raise Http404
