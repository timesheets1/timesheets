from django.contrib.auth.decorators import login_required
from django.core.exceptions import PermissionDenied
from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import get_object_or_404
from django.template import loader
from django.urls import reverse

from management.forms import ContractPeriodForm
from management.models import ContractPeriod, Employment


@login_required(login_url='account_login')
def create_contract_period(request, employment_id):
    '''
    use this in order to create an employment for yourself in a new or present company based on the name.
    :param request:
    :return:
    '''
    template = loader.get_template('contract_period_create.html')
    context = {}

    context["request_method"] = request.method

    employment = get_object_or_404(Employment, id=employment_id)
    if not employment.is_admin_for_company:
        raise PermissionDenied

    context["employment"] = employment

    if request.method == "POST":

        form = ContractPeriodForm(request.POST)
        context["form"] = form

        if form.is_valid():
            period = ContractPeriod(
                start=form.cleaned_data["start"],
                end=form.cleaned_data["end"],
                work_hours_amount=form.cleaned_data["work_hours_amount"],
                holiday_days_amount=form.cleaned_data["holiday_days_amount"],
                monday=form.cleaned_data["monday"],
                tuesday=form.cleaned_data["tuesday"],
                wednesday=form.cleaned_data["wednesday"],
                thursday=form.cleaned_data["thursday"],
                friday=form.cleaned_data["friday"],
                saturday=form.cleaned_data["saturday"],
                sunday=form.cleaned_data["sunday"],
                employment=employment,
            )
            period.save()

            return HttpResponseRedirect(reverse("management_dashboard"))
    else:
        form = ContractPeriodForm()
        context["form"] = form
    return HttpResponse(template.render(context, request))


@login_required(login_url='account_login')
def edit_contract_period(request, employment_id, pk):
    template = loader.get_template('contract_period_edit_delete.html')
    context = {}

    employment = get_object_or_404(Employment, id=employment_id)
    if not employment.is_admin_for_company:
        raise PermissionDenied
    context["employment"] = employment

    period = get_object_or_404(ContractPeriod, pk=pk, employment=employment)
    context["period"] = period

    if request.method == "POST":

        form = ContractPeriodForm(request.POST)
        context["form"] = form
        if 'delete' in request.POST:
            period.delete()
            return HttpResponseRedirect(reverse("management_dashboard"))

        if form.is_valid():
            period.start = form.cleaned_data["start"]
            period.end = form.cleaned_data["end"]
            period.work_hours_amount = form.cleaned_data["work_hours_amount"]
            period.holiday_days_amount = form.cleaned_data["holiday_days_amount"]
            period.monday = form.cleaned_data["monday"]
            period.tuesday = form.cleaned_data["tuesday"]
            period.wednesday = form.cleaned_data["wednesday"]
            period.thursday = form.cleaned_data["thursday"]
            period.friday = form.cleaned_data["friday"]
            period.saturday = form.cleaned_data["saturday"]
            period.sunday = form.cleaned_data["sunday"]
            period.save()

            return HttpResponseRedirect(reverse("management_dashboard"))
    else:
        form = ContractPeriodForm(initial={
            "start": period.start,
            "end": period.end,
            "work_hours_amount": period.work_hours_amount,
            "holiday_days_amount": period.holiday_days_amount,
            "monday": period.monday,
            "tuesday": period.tuesday,
            "wednesday": period.wednesday,
            "thursday": period.thursday,
            "friday": period.friday,
            "saturday": period.saturday,
            "sunday": period.sunday,
        })
        context["form"] = form

    return HttpResponse(template.render(context, request))


