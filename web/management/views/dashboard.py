from django.contrib.auth.decorators import login_required
from django.http import HttpResponse
from django.template import loader

from management.models import ContractPeriod
from management.utils import employment_period_integrity_check
from timesheets.helper import get_employments_for_user


@login_required(login_url='account_login')
def dashboard(request):
    template = loader.get_template('dashboard.html')
    context = {}

    employments = get_employments_for_user(user=request.user)
    context["employments"] = employments

    admin_company_employments = employments.filter(is_admin_for_company=True)
    context["admin_company_employments"] = admin_company_employments

    data_by_emploment = []

    for employment in employments:
        data = {}

        ######################################
        # Contract Periods

        periods = ContractPeriod.objects.filter(employment=employment).order_by("start")

        data["contract_periods"] = periods
        data["employment"] = employment

        contract_period_issues = employment_period_integrity_check(employment)
        data["contract_period_issues"] = contract_period_issues
        data_by_emploment.append(data)

    context["data_by_emploment"] = data_by_emploment

    return HttpResponse(template.render(context, request))

