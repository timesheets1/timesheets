from django.contrib.auth.decorators import login_required
from django.http import HttpResponse
from django.template import loader

from management.utils import overlapping_intervals_integrity_check, interval_integrity_check


@login_required(login_url='account_login')
def interval_integrity(request):
    template = loader.get_template('interval_integrity.html')
    context = {}
    user = request.user

    context["user"] = user

    overlapping_issues = overlapping_intervals_integrity_check(request.user)
    context["overlapping_issues"] = overlapping_issues
    interval_issues = interval_integrity_check(request.user)
    context["interval_issues"] = interval_issues

    issues_count = len(overlapping_issues) + len(interval_issues)
    context["issues_count"] = issues_count

    return HttpResponse(template.render(context, request))

