from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import HttpResponse
from django.template import loader
from django.utils.translation import gettext_lazy
from django.views import View

from management.forms import IntervalChangeCategoriesForm
from management.models import Profile
from timesheets.models import Interval


class IntervalChangeCategories(LoginRequiredMixin, View):

    def get(self, request):

        template = loader.get_template('interval_change_categories.html')
        context = {}

        profile = Profile.objects.filter(user=request.user).first()

        form = IntervalChangeCategoriesForm(profile=profile)
        context["form"] = form

        return HttpResponse(template.render(context, request))


    def post(self, request):


        template = loader.get_template('interval_change_categories.html')
        context = {}

        profile = Profile.objects.filter(user=request.user).first()

        form = IntervalChangeCategoriesForm(profile=profile, data=request.POST)
        context["form"] = form

        if form.is_valid():
            old_category = form.cleaned_data.get("old_category")
            old_subcategory = form.cleaned_data.get("old_subcategory")
            new_category = form.cleaned_data.get("new_category")
            new_subcategory = form.cleaned_data.get("new_subcategory")

            intervals = Interval.objects.filter(
                category=old_category,
                subcategory=old_subcategory,
                user=request.user,
            )



            count = intervals.update(
                category=new_category,
                subcategory=new_subcategory,
            )

            messages.success(request, gettext_lazy(f"{count} intervals have been updated."))

        return HttpResponse(template.render(context, request))
