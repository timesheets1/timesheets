from django.conf import settings
from django.http import HttpResponse
from django.template import loader


def impressum(request):
    template = loader.get_template("impressum.html")

    context = {
        'impressum_name': settings.IMPRESSUM_NAME,
        'impressum_address': settings.IMPRESSUM_ADDRESS,
        'impressum_contact': settings.IMPRESSUM_CONTACT,
    }

    return HttpResponse(template.render(context, request))
