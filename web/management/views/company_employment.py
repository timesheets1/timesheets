from django.contrib.auth.decorators import login_required
from django.db.models import Max
from django.http import HttpResponseRedirect, HttpResponse, Http404
from django.shortcuts import get_object_or_404
from django.template import loader
from django.urls import reverse

from holidays.models import HolidayEvent
from management.forms import EmploymentAdminCreateForm, EmploymentAdminEditForm, EmploymentDeleteForm
from management.models import Company, Profile, Employment, ContractPeriod
from management.utils import employment_period_integrity_check
from timesheets.models import Interval


@login_required(login_url='account_login')
def company_create_employment(request, company_pk):
    '''
    use this in order to create an employment as admin for another user in your company.
    :param request:
    :return:
    '''
    template = loader.get_template("employment_create.html")
    context = {}

    company = get_object_or_404(Company, id=company_pk)
    context["company"] = company
    profile = Profile.objects.filter(user=request.user).first()

    employment = Employment.objects.filter(
        profile=profile,
        company=company,
    ).first()

    user_ids_already_employed_here = []
    all_employments_in_company = Employment.objects.filter(
        company=company,
    )
    for element in all_employments_in_company:
        if element.profile.user.id not in user_ids_already_employed_here:
            user_ids_already_employed_here.append(element.profile.user.id)

    if employment.is_admin_for_company:

        if request.method == "POST":
            form = EmploymentAdminCreateForm(request.POST,
                                             user_ids_already_employed_here=user_ids_already_employed_here)
            context["form"] = form

            if form.is_valid():

                employment_profile = Profile.objects.filter(user=form.cleaned_data["user"]).first()
                employment = Employment(
                    profile=employment_profile,
                    company=company,
                    start=form.cleaned_data["start"],
                    end=form.cleaned_data["end"],
                )

                max_order = Employment.objects.filter(profile=employment_profile).aggregate(max_order=Max('order'))[
                    "max_order"]

                if max_order:
                    employment.order = max_order + 1

                employment.save()

                return HttpResponseRedirect(reverse("management_company_detail", args=[company_pk]))

        else:
            form = EmploymentAdminCreateForm(user_ids_already_employed_here=user_ids_already_employed_here)
            context["form"] = form

        return HttpResponse(template.render(context, request))
    else:
        raise Http404


@login_required(login_url='account_login')
def company_detail_employment(request, company_pk, employment_pk):
    template = loader.get_template('company_employment_detail.html')
    context = {}

    company = get_object_or_404(Company, id=company_pk)
    context["company"] = company

    profile = Profile.objects.filter(user=request.user).first()

    request_users_employment = Employment.objects.filter(
        profile=profile,
        company=company,
    ).first()

    employment = get_object_or_404(Employment, id=employment_pk)
    context["employment"] = employment

    contract_periods = ContractPeriod.objects.filter(
        employment=employment,
    )
    context["contract_periods"] = contract_periods

    contract_period_issues = employment_period_integrity_check(employment)
    context["contract_period_issues"] = contract_period_issues

    if request_users_employment.is_admin_for_company:
        return HttpResponse(template.render(context, request))
    else:
        raise Http404


@login_required(login_url='account_login')
def company_edit_employment(request, company_pk, employment_pk):
    template = loader.get_template('company_employment_edit.html')
    context = {}

    company = get_object_or_404(Company, id=company_pk)
    context["company"] = company

    profile = Profile.objects.filter(user=request.user).first()

    employment = Employment.objects.filter(
        profile=profile,
        company=company,
    ).first()

    employment_to_edit = get_object_or_404(Employment, id=employment_pk)
    context["employment"] = employment_to_edit

    if employment.is_admin_for_company:

        if request.method == "POST":

            form = EmploymentAdminEditForm(request.POST)
            context["form"] = form

            if form.is_valid():
                employment_to_edit.start = form.cleaned_data["start"]
                employment_to_edit.end = form.cleaned_data["end"]
                employment_to_edit.usage_start = form.cleaned_data["usage_start"]
                employment_to_edit.offset_overtime_hour_account = form.cleaned_data["offset_overtime_hour_account"]
                employment_to_edit.offset_holiday_days = form.cleaned_data["offset_holiday_days"]
                employment_to_edit.is_admin_for_company = form.cleaned_data["is_admin_for_company"]
                employment_to_edit.save()

                return HttpResponseRedirect(
                    reverse("management_company_detail_employment", args=[company_pk, employment_to_edit.pk]))
        else:
            form = EmploymentAdminEditForm(initial={
                "start": employment_to_edit.start,
                "end": employment_to_edit.end,
                "usage_start": employment_to_edit.usage_start,
                "offset_overtime_hour_account": employment_to_edit.offset_overtime_hour_account,
                "offset_holiday_days": employment_to_edit.offset_holiday_days,
                "is_admin_for_company": employment_to_edit.is_admin_for_company,
            })
            context["form"] = form
        return HttpResponse(template.render(context, request))

    else:
        raise Http404


@login_required(login_url='account_login')
def company_delete_employment(request, company_pk, employment_pk):
    '''
    used to delete all data associated to an employment in the company the user adminsters
    :param request:
    :param pk:
    :return:
    '''
    template = loader.get_template('company_employment_delete.html')
    context = {}

    company = get_object_or_404(Company, id=company_pk)
    context["company"] = company
    profile = Profile.objects.filter(user=request.user).first()

    employment = Employment.objects.filter(
        profile=profile,
        company=company,
    ).first()

    employment_to_delete = get_object_or_404(Employment, id=employment_pk)
    context["employment"] = employment_to_delete

    form = EmploymentDeleteForm(request.POST, instance=employment_to_delete)
    if employment.is_admin_for_company:

        if request.method == "POST":
            if form.is_valid():  # checks CSRF

                Interval.objects.filter(category__company=employment_to_delete.company,
                                        user=employment_to_delete.profile.user).delete()
                HolidayEvent.objects.filter(employment=employment_to_delete).delete()
                # holiday days cascade from holiday event and interval.

                employment_to_delete.delete()
                # contract periods cascade from employment
                return HttpResponseRedirect(reverse("management_company_detail", args=[company_pk]))

        return HttpResponse(template.render(context, request))

    else:
        raise Http404
