from django.core.exceptions import PermissionDenied
from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import get_object_or_404
from django.template import loader
from django.urls import reverse

from management.forms import ContractPeriodForm
from management.models import Company, Profile, Employment, ContractPeriod


def company_contract_period_create(request, company_pk, employment_pk):
    template = loader.get_template("contract_period_create.html")
    context = {}
    company = get_object_or_404(Company, id=company_pk)
    context["company"] = company
    profile = Profile.objects.filter(user=request.user).first()

    request_user_employment = Employment.objects.filter(
        profile=profile,
        company=company,
    ).first()
    if not request_user_employment.is_admin_for_company:
        raise PermissionDenied

    employment = get_object_or_404(Employment, id=employment_pk)

    if request.method == "POST":
        form = ContractPeriodForm(request.POST)
        if form.is_valid():
            period = ContractPeriod(
                start=form.cleaned_data["start"],
                end=form.cleaned_data["end"],
                work_hours_amount=form.cleaned_data["work_hours_amount"],
                holiday_days_amount=form.cleaned_data["holiday_days_amount"],
                monday=form.cleaned_data["monday"],
                tuesday=form.cleaned_data["tuesday"],
                wednesday=form.cleaned_data["wednesday"],
                thursday=form.cleaned_data["thursday"],
                friday=form.cleaned_data["friday"],
                saturday=form.cleaned_data["saturday"],
                sunday=form.cleaned_data["sunday"],
                employment=employment,
            )
            period.save()
            return HttpResponseRedirect(
                reverse("management_company_detail_employment", args=[company_pk, employment_pk]))

    else:
        form = ContractPeriodForm()
    context["form"] = form

    return HttpResponse(template.render(context, request))


def company_contract_period_edit_delete(request, company_pk, employment_pk, contract_period_pk):
    template = loader.get_template("contract_period_edit_delete.html")
    context = {}
    company = get_object_or_404(Company, id=company_pk)
    context["company"] = company
    profile = Profile.objects.filter(user=request.user).first()

    request_user_employment = Employment.objects.filter(
        profile=profile,
        company=company,
    ).first()
    if not request_user_employment.is_admin_for_company:
        raise PermissionDenied

    employment = get_object_or_404(Employment, id=employment_pk)
    context["employment"] = employment

    contract_period = get_object_or_404(ContractPeriod, id=contract_period_pk)

    if request.method == "POST":
        form = ContractPeriodForm(request.POST)
        if "delete" in request.POST:
            contract_period.delete()
            return HttpResponseRedirect(
                reverse("management_company_detail_employment", args=[company_pk, employment_pk]))

        if form.is_valid():
            contract_period.start = form.cleaned_data["start"]
            contract_period.end = form.cleaned_data["end"]
            contract_period.work_hours_amount = form.cleaned_data["work_hours_amount"]
            contract_period.holiday_days_amount = form.cleaned_data["holiday_days_amount"]
            contract_period.monday = form.cleaned_data["monday"]
            contract_period.tuesday = form.cleaned_data["tuesday"]
            contract_period.wednesday = form.cleaned_data["wednesday"]
            contract_period.thursday = form.cleaned_data["thursday"]
            contract_period.friday = form.cleaned_data["friday"]
            contract_period.saturday = form.cleaned_data["saturday"]
            contract_period.sunday = form.cleaned_data["sunday"]
            contract_period.save()

            return HttpResponseRedirect(
                reverse("management_company_detail_employment", args=[company_pk, employment_pk]))


    else:
        form = ContractPeriodForm(initial={
            "start": contract_period.start,
            "end": contract_period.end,
            "work_hours_amount": contract_period.work_hours_amount,
            "holiday_days_amount": contract_period.holiday_days_amount,
            "monday": contract_period.monday,
            "tuesday": contract_period.tuesday,
            "wednesday": contract_period.wednesday,
            "thursday": contract_period.thursday,
            "friday": contract_period.friday,
            "saturday": contract_period.saturday,
            "sunday": contract_period.sunday,
        })

    context["form"] = form
    return HttpResponse(template.render(context, request))

