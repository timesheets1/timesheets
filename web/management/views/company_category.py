from django.contrib import messages
from django.http import HttpResponseRedirect, HttpResponse, Http404
from django.shortcuts import get_object_or_404
from django.template import loader
from django.urls import reverse
from django.utils.translation import gettext_lazy

from management.forms import CategoryForm, CategoryDeleteForm
from management.models import Company, Profile, Employment
from timesheets.models import Category, Interval


def company_create_category(request, company_pk):
    template = loader.get_template("category_create.html")
    context = {}

    company = get_object_or_404(Company, id=company_pk)
    profile = Profile.objects.filter(user=request.user).first()

    employment = Employment.objects.filter(
        profile=profile,
        company=company,
    ).first()

    if employment.is_admin_for_company:
        context["company"] = company
        context["employment"] = employment
        form = CategoryForm(company=employment.company)
        context["form"] = form

        if request.method == "POST":

            form = CategoryForm(data=request.POST, company=employment.company)
            context["form"] = form
            if form.is_valid():
                form.save()

            return HttpResponseRedirect(reverse("management_company_detail", args=[company_pk]))

        return HttpResponse(template.render(context, request))

    else:
        raise Http404


def company_update_category(request, company_pk, category_pk):
    template = loader.get_template("category_edit.html")
    context = {}

    company = get_object_or_404(Company, id=company_pk)
    category = get_object_or_404(Category, id=category_pk)
    profile = Profile.objects.filter(user=request.user).first()

    employment = Employment.objects.filter(
        profile=profile,
        company=company,
    ).first()

    if employment.is_admin_for_company:
        context["company"] = company
        context["employment"] = employment
        context["category"] = category
        interval_usage = Interval.objects.filter(category=category).count()

        context["interval_usage"] = interval_usage

        if request.method == "POST":

            form = CategoryForm(data=request.POST, instance=category, company=employment.company)
            context["form"] = form
            if form.is_valid():
                form.save()
                return HttpResponseRedirect(reverse("management_company_detail", args=[company_pk]))
            return HttpResponse(template.render(context, request))

        form = CategoryForm(instance=category, company=employment.company)
        context["form"] = form

        return HttpResponse(template.render(context, request))

    else:
        raise Http404


def company_delete_category(request, company_pk, category_pk):
    template = loader.get_template("category_delete.html")
    context = {}

    company = get_object_or_404(Company, id=company_pk)
    category = get_object_or_404(Category, id=category_pk)
    profile = Profile.objects.filter(user=request.user).first()

    employment = Employment.objects.filter(
        profile=profile,
        company=company,
    ).first()

    if employment.is_admin_for_company:
        context["company"] = company
        context["employment"] = employment
        context["category"] = category
        interval_usage = Interval.objects.filter(category=category).count()

        context["interval_usage"] = interval_usage
        if request.method == "POST":
            if interval_usage > 0:
                raise Http404
            form = CategoryDeleteForm(request.POST)
            context["form"] = form
            if form.is_valid():
                category.delete()
                messages.success(request, gettext_lazy("The category was deleted."))
                return HttpResponseRedirect(reverse("management_company_detail", args=[company_pk]))
            return HttpResponse(template.render(context, request))

        form = CategoryDeleteForm()
        context["form"] = form

        return HttpResponse(template.render(context, request))

    else:
        raise Http404
