from django.contrib import messages
from django.http import HttpResponseRedirect, HttpResponse, Http404
from django.shortcuts import get_object_or_404
from django.template import loader
from django.urls import reverse

from management.forms import PublicHolidayForm
from management.models import Profile, Employment, Company, PublicHoliday


def company_create_public_holiday(request, company_pk):
    template = loader.get_template("public_holdiay_create.html")
    context = {}

    company = get_object_or_404(Company, id=company_pk)
    profile = Profile.objects.filter(user=request.user).first()

    employment = Employment.objects.filter(
        profile=profile,
        company=company,
    ).first()

    if employment.is_admin_for_company:
        context["company"] = company
        context["employment"] = employment
        form = PublicHolidayForm(company=company)
        context["form"] = form

        if request.method == "POST":

            form = PublicHolidayForm(data=request.POST, company=company)
            context["form"] = form
            if form.is_valid():
                public_holiday = form.save()
                messages.success(request, f"Public Holiday {public_holiday.name} has been deleted.")

            return HttpResponseRedirect(reverse("management_company_detail", args=[company_pk]))

        return HttpResponse(template.render(context, request))

    else:
        raise Http404


def company_update_public_holdiay(request, company_pk, public_holiday_pk):
    template = loader.get_template("public_holiday_edit.html")
    context = {}

    company = get_object_or_404(Company, id=company_pk)
    public_holiday = get_object_or_404(PublicHoliday, id=public_holiday_pk)
    profile = Profile.objects.filter(user=request.user).first()

    employment = Employment.objects.filter(
        profile=profile,
        company=company,
    ).first()

    if employment.is_admin_for_company:
        context["company"] = company
        context["employment"] = employment
        context["public_holiday"] = public_holiday

        if request.method == "POST":

            form = PublicHolidayForm(data=request.POST, instance=public_holiday, company=company)
            context["form"] = form
            if form.is_valid():
                form.save()
                messages.success(request, f"Public Holiday {public_holiday.name} has been updated.")
                return HttpResponseRedirect(reverse("management_company_detail", args=[company_pk]))
            return HttpResponse(template.render(context, request))

        form = PublicHolidayForm(instance=public_holiday, company=company)
        context["form"] = form

        return HttpResponse(template.render(context, request))

    else:
        raise Http404


def company_delete_public_holiday(request, company_pk, public_holiday_pk):
    template = loader.get_template("public_holiday_delete.html")
    context = {}

    company = get_object_or_404(Company, id=company_pk)
    public_holiday = get_object_or_404(PublicHoliday, id=public_holiday_pk)
    profile = Profile.objects.filter(user=request.user).first()

    employment = Employment.objects.filter(
        profile=profile,
        company=company,
    ).first()

    if employment.is_admin_for_company:
        context["company"] = company
        context["employment"] = employment
        context["public_holiday"] = public_holiday
        if request.method == "POST":
            public_holiday.delete()
            messages.success(request, f"Public Holiday {public_holiday.name} has been deleted.")
            return HttpResponseRedirect(reverse("management_company_detail", args=[company_pk]))
        return HttpResponse(template.render(context, request))

    else:
        raise Http404


