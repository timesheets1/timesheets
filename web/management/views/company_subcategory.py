from django.contrib import messages
from django.http import HttpResponseRedirect, HttpResponse, Http404
from django.shortcuts import get_object_or_404
from django.template import loader
from django.urls import reverse
from django.utils.translation import gettext_lazy

from management.forms import SubcategoryForm, SubcategoryDeleteForm
from management.models import Company, Profile, Employment
from timesheets.models import Subcategory, Interval


def company_create_subcategory(request, company_pk):
    template = loader.get_template("subcategory_create.html")
    context = {}

    company = get_object_or_404(Company, id=company_pk)
    profile = Profile.objects.filter(user=request.user).first()

    employment = Employment.objects.filter(
        profile=profile,
        company=company,
    ).first()

    if employment.is_admin_for_company:
        context["company"] = company
        context["employment"] = employment
        form = SubcategoryForm(company=employment.company)
        context["form"] = form

        if request.method == "POST":

            form = SubcategoryForm(data=request.POST, company=employment.company)
            context["form"] = form
            if form.is_valid():
                form.save()

            return HttpResponseRedirect(reverse("management_company_detail", args=[company_pk]))

        return HttpResponse(template.render(context, request))

    else:
        raise Http404


def company_update_subcategory(request, company_pk, subcategory_pk):
    template = loader.get_template("subcategory_edit.html")
    context = {}

    company = get_object_or_404(Company, id=company_pk)
    subcategory = get_object_or_404(Subcategory, id=subcategory_pk)
    profile = Profile.objects.filter(user=request.user).first()

    employment = Employment.objects.filter(
        profile=profile,
        company=company,
    ).first()

    if employment.is_admin_for_company:
        context["company"] = company
        context["employment"] = employment
        context["subcategory"] = subcategory
        interval_usage = Interval.objects.filter(subcategory=subcategory).count()

        context["interval_usage"] = interval_usage

        if request.method == "POST":

            form = SubcategoryForm(data=request.POST, instance=subcategory, company=employment.company)
            context["form"] = form
            if form.is_valid():
                form.save()
                return HttpResponseRedirect(reverse("management_company_detail", args=[company_pk]))
            return HttpResponse(template.render(context, request))

        form = SubcategoryForm(instance=subcategory, company=employment.company)
        context["form"] = form

        return HttpResponse(template.render(context, request))

    else:
        raise Http404


def company_delete_subcategory(request, company_pk, subcategory_pk):
    template = loader.get_template("subcategory_delete.html")
    context = {}

    company = get_object_or_404(Company, id=company_pk)
    subcategory = get_object_or_404(Subcategory, id=subcategory_pk)
    profile = Profile.objects.filter(user=request.user).first()

    employment = Employment.objects.filter(
        profile=profile,
        company=company,
    ).first()

    if employment.is_admin_for_company:
        context["company"] = company
        context["employment"] = employment
        context["subcategory"] = subcategory
        interval_usage = Interval.objects.filter(subcategory=subcategory).count()

        context["interval_usage"] = interval_usage
        if request.method == "POST":
            if interval_usage > 0:
                raise Http404
            form = SubcategoryDeleteForm(request.POST)
            context["form"] = form
            if form.is_valid():
                subcategory.delete()
                messages.success(request, gettext_lazy("The subcategory was deleted."))
                return HttpResponseRedirect(reverse("management_company_detail", args=[company_pk]))
            return HttpResponse(template.render(context, request))

        form = SubcategoryDeleteForm()
        context["form"] = form

        return HttpResponse(template.render(context, request))

    else:
        raise Http404
