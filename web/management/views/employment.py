from django.contrib.auth.decorators import login_required
from django.db.models import Max
from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import get_object_or_404
from django.template import loader
from django.urls import reverse
from django.utils.translation import gettext_lazy

from holidays.models import HolidayEvent
from management.forms import EmploymentPersonalCreateForm, EmploymentEditForm, EmploymentDeleteForm
from management.models import Company, Profile, Employment
from timesheets.models import Category, Subcategory, Interval


@login_required(login_url='account_login')
def create_employment(request):
    template = loader.get_template("employment_create.html")
    context = {}
    if request.method == "POST":
        form = EmploymentPersonalCreateForm(request.POST)
        context["form"] = form

        if form.is_valid():
            company, created = Company.objects.get_or_create(
                name=form.cleaned_data["company"]
            )

            if created:
                #pre-fill some categories and subcategories
                category_1 = Category(
                    name=gettext_lazy("Project 1"),
                    company=company,
                    bookable=True,
                    exclusive=True,
                    working_time=True,
                    description=gettext_lazy("Work on Project 1"),
                )
                category_1.save()

                category_2 = Category(
                    name=gettext_lazy("Project 2"),
                    company=company,
                    bookable=True,
                    exclusive=True,
                    working_time=True,
                    description=gettext_lazy("Work on Project 2"),
                )
                category_2.save()

                category_3 = Category(
                    name=gettext_lazy("Internal"),
                    company=company,
                    bookable=True,
                    exclusive=True,
                    working_time=True,
                    description=gettext_lazy("Work on internal projects"),
                )
                category_3.save()

                subcategory_1 = Subcategory(
                    name=gettext_lazy("Project Management"),
                    company=company,
                    bookable=True,
                )
                subcategory_1.save()

                subcategory_2 = Subcategory(
                    name=gettext_lazy("Software Development"),
                    company=company,
                    bookable=True,
                )
                subcategory_2.save()

                subcategory_3 = Subcategory(
                    name=gettext_lazy("Meetings"),
                    company=company,
                    bookable=True,
                    description=gettext_lazy("Personal or remote Meetings with the client.")
                )
                subcategory_3.save()


            profile = Profile.objects.filter(user=request.user).first()
            max_order = Employment.objects.filter(profile=profile).aggregate(max_order=Max('order'))["max_order"]

            employment = Employment(
                company=company,
                start=form.cleaned_data["start"],
                end=form.cleaned_data["end"],
                profile=profile,
            )

            if max_order:
                employment.order = max_order + 1

            if created:
                employment.is_admin_for_company = True

            employment.save()

            return HttpResponseRedirect(reverse("management_dashboard"))

    else:
        form = EmploymentPersonalCreateForm()
        context["form"] = form

    return HttpResponse(template.render(context, request))


@login_required(login_url='account_login')
def edit_employment(request, pk):
    template = loader.get_template('employment_edit.html')
    context = {}

    employment = get_object_or_404(Employment, id=pk)
    context["employment"] = employment

    if request.method == "POST":

        form = EmploymentEditForm(request.POST)
        context["form"] = form

        if form.is_valid():
            employment.start = form.cleaned_data["start"]
            employment.end = form.cleaned_data["end"]
            employment.usage_start = form.cleaned_data["usage_start"]
            employment.offset_overtime_hour_account = form.cleaned_data["offset_overtime_hour_account"]
            employment.offset_holiday_days = form.cleaned_data["offset_holiday_days"]

            employment.save()
            return HttpResponseRedirect(reverse("management_dashboard"))
    else:
        form = EmploymentEditForm(initial={
            "start": employment.start,
            "end": employment.end,
            "usage_start": employment.usage_start,
            "offset_overtime_hour_account": employment.offset_overtime_hour_account,
            "offset_holiday_days": employment.offset_holiday_days,
        })
        context["form"] = form

    return HttpResponse(template.render(context, request))


@login_required(login_url='account_login')
def delete_employment(request, pk):
    '''
    used to delete all data associated to an own employment
    :param request:
    :param pk:
    :return:
    '''
    template = loader.get_template('employment_delete.html')
    context = {}

    employment = get_object_or_404(Employment, id=pk)
    context["employment"] = employment

    form = EmploymentDeleteForm(request.POST, instance=employment)

    if request.method == "POST":
        if form.is_valid():  # checks CSRF

            Interval.objects.filter(category__company=employment.company, user=employment.profile.user).delete()
            HolidayEvent.objects.filter(employment=employment).delete()
            # holiday days cascade from holiday event and interval.

            employment.delete()
            # contract periods cascade from employment

            return HttpResponseRedirect(
                reverse("management_dashboard"))  # wherever to go after deleting

    return HttpResponse(template.render(context, request))
