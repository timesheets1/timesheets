from django.urls import path

from management.views.company_category import company_create_category, company_update_category, company_delete_category
from management.views.company_contract_period import company_contract_period_create, company_contract_period_edit_delete
from management.views.company_detail import company_detail_view
from management.views.company_employment import company_create_employment, company_edit_employment, \
    company_detail_employment, company_delete_employment
from management.views.company_public_holidays import company_create_public_holiday, company_update_public_holdiay, \
    company_delete_public_holiday
from management.views.company_subcategory import company_create_subcategory, company_update_subcategory, \
    company_delete_subcategory
from management.views.contract_period import create_contract_period, edit_contract_period
from management.views.dashboard import dashboard
from management.views.employment import create_employment, edit_employment, delete_employment
from management.views.interval_change_categories import IntervalChangeCategories
from management.views.interval_integrity import interval_integrity

urlpatterns = [
    path('dashboard/', dashboard, name='management_dashboard'),
    path('interval_integrity/', interval_integrity, name='management_interval_integrity'),
    path('management_create_contract_period/<employment_id>/', create_contract_period,
         name='management_create_contract_period'),
    path('management_edit_contract_period/<employment_id>/<pk>/', edit_contract_period,
         name='management_edit_contract_period'),
    path('management/employment/create', create_employment, name='management_employment_create'),
    path('management/employment/<int:pk>', edit_employment, name='management_employment_edit'),
    path('management/employment/<int:pk>/delete', delete_employment, name='management_employment_delete'),
    path('management/company/<int:company_pk>', company_detail_view, name="management_company_detail"),

    path('management/company/<int:company_pk>/category/create', company_create_category,
         name="management_company_create_category"),
    path('management/company/<int:company_pk>/category/<int:category_pk>', company_update_category,
         name="management_company_update_category"),
    path('management/company/<int:company_pk>/category/<int:category_pk>/delete', company_delete_category,
         name="management_company_delete_category"),

    path('management/company/<int:company_pk>/subcategory/create', company_create_subcategory,
         name="management_company_create_subcategory"),
    path('management/company/<int:company_pk>/subcategory/<int:subcategory_pk>', company_update_subcategory,
         name="management_company_update_subcategory"),
    path('management/company/<int:company_pk>/subcategory/<int:subcategory_pk>/delete',
         company_delete_subcategory, name="management_company_delete_subcategory"),

    path('management/company/<int:company_pk>/public_holiday/create', company_create_public_holiday,
         name="management_company_create_public_holiday"),
    path('management/company/<int:company_pk>/public_holiday/<public_holiday_pk>',
         company_update_public_holdiay, name="management_company_update_public_holiday"),
    path('management/company/<int:company_pk>/public_holiday/<public_holiday_pk>/delete',
         company_delete_public_holiday, name="management_company_delete_public_holiday"),

    path('management/company/<int:company_pk>/employment/create', company_create_employment,
         name="management_company_create_employment"),
    path('management/company/<int:company_pk>/employment/<int:employment_pk>', company_edit_employment,
         name="management_company_edit_employment"),
    path('management/company/<int:company_pk>/employment/<int:employment_pk>/detail', company_detail_employment,
         name="management_company_detail_employment"),
    path('management/company/<int:company_pk>/employment/<int:employment_pk>/delete', company_delete_employment,
         name="management_company_delete_employment"),

    path('management/company/<int:company_pk>/employment/<int:employment_pk>/contract_period/create',
         company_contract_period_create, name="company_contract_period_create"),
    path('management/company/<int:company_pk>/employment/<int:employment_pk>/contract_period/<int:contract_period_pk>',
         company_contract_period_edit_delete, name="company_contract_period_edit_delete"),

    path("management/interval/change_categories", IntervalChangeCategories.as_view(), name="interval_change_categories"),
]
