from django.contrib import admin

from management.models import Profile, ContractPeriod, Employment, PublicHoliday


class ContractPeriodInLine(admin.TabularInline):
	model = ContractPeriod
	extra = 0


@admin.register(PublicHoliday)
class PublicHolidayAdmin(admin.ModelAdmin):
	list_display = ("name","date", "company")
	list_filter = ("company",)
	search_fields = ("name", )
	


@admin.register(Profile)
class ProfileAdmin(admin.ModelAdmin):
	list_display = ('user', )


@admin.register(ContractPeriod)
class ContractPeriodAdmin(admin.ModelAdmin):
	list_display = ('start', 'end', 'work_hours_amount', 'holiday_days_amount', 'employment')
	list_filter = ('employment',)


@admin.register(Employment)
class EmploymentAdmin(admin.ModelAdmin):
	list_display = ('profile', 'company', 'start', 'end', 'is_admin_for_company', 'order')
	list_filter = ('profile', 'company', 'is_admin_for_company')
	inlines = [ContractPeriodInLine]
