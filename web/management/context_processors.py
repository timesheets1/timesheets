from management.models import Employment, Company
from timesheets.helper import get_employments_for_user, get_companies_for_user


def employments_for_user_processor(request):
    context = {}
    employments = Employment.objects.none()
    if request.user.is_authenticated:
        employments = get_employments_for_user(user=request.user)
    context["users_employments"] = employments
    admin_company_employments = employments.filter(is_admin_for_company=True)
    context["admin_company_employments"] = admin_company_employments
    return context
