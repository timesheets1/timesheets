# Generated by Django 3.0.4 on 2020-09-29 19:16

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('management', '0008_auto_20200906_1814'),
    ]

    operations = [
        migrations.AddField(
            model_name='workhourscontractedperiod',
            name='friday',
            field=models.BooleanField(default=True),
        ),
        migrations.AddField(
            model_name='workhourscontractedperiod',
            name='monday',
            field=models.BooleanField(default=True),
        ),
        migrations.AddField(
            model_name='workhourscontractedperiod',
            name='saturday',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='workhourscontractedperiod',
            name='sunday',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='workhourscontractedperiod',
            name='thursday',
            field=models.BooleanField(default=True),
        ),
        migrations.AddField(
            model_name='workhourscontractedperiod',
            name='tuesday',
            field=models.BooleanField(default=True),
        ),
        migrations.AddField(
            model_name='workhourscontractedperiod',
            name='wednesday',
            field=models.BooleanField(default=True),
        ),
    ]
