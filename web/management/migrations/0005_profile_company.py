# Generated by Django 3.0.4 on 2020-09-01 18:48

import django.db.models.deletion
from django.db import migrations, models


class Migration(migrations.Migration):
	dependencies = [
		('timesheets', '0006_auto_20200901_2044'),
		('management', '0004_auto_20191221_1616'),
	]

	operations = [
		migrations.AddField(
			model_name='profile',
			name='company',
			field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL,
									to='timesheets.Company'),
		),
	]
