# Generated by Django 3.0.4 on 2020-09-06 16:06

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('management', '0006_auto_20200905_1711'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='holidaydayscontractedperiod',
            name='profile',
        ),
        migrations.RemoveField(
            model_name='workhourscontractedperiod',
            name='profile',
        ),
        migrations.AddField(
            model_name='holidaydayscontractedperiod',
            name='employment',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='management.Employment'),
        ),
        migrations.AddField(
            model_name='workhourscontractedperiod',
            name='employment',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='management.Employment'),
        ),
    ]
