from django import forms
from django.forms.models import inlineformset_factory
from django.utils.translation import gettext_lazy

from holidays.models import HolidayDay, HolidayEvent
from webTimesheets.custom_form_fields import DateInput


class HolidayDayForm(forms.ModelForm):
    date = forms.DateField(label=gettext_lazy("Date"), widget=DateInput(), required=True)

    class Meta:
        model = HolidayDay
        exclude = ["user"]


HolidayDayCreateFormSet = inlineformset_factory(
    HolidayEvent, HolidayDay, form=HolidayDayForm,
    fields=['date', 'weight', 'description'], extra=5, can_delete=False
)

HolidayDayUpdateFormSet = inlineformset_factory(
    HolidayEvent, HolidayDay, form=HolidayDayForm,
    fields=['date', 'weight', 'description'], extra=1, can_delete=True
)


class HolidayEventForm(forms.ModelForm):
    class Meta:
        model = HolidayEvent
        exclude = ["employment"]


class HolodayEventCreateForm(forms.Form):
    description = forms.CharField(label=gettext_lazy("Description"), required=True)
    start = forms.DateField(label=gettext_lazy("Start"), widget=DateInput(), input_formats=["%Y-%m-%d", "%d.%m.%Y"],
                            required=True)
    end = forms.DateField(label=gettext_lazy("End"), widget=DateInput(), input_formats=["%Y-%m-%d", "%d.%m.%Y"],
                          required=True)
