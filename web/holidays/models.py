import datetime

from django.db import models
from django.utils import timezone
from django.utils.translation import gettext_lazy

from management.models import Employment
from timesheets.models import Interval


class HolidayEvent(models.Model):
    description = models.CharField(verbose_name=gettext_lazy("Description"), max_length=200, blank=True, default='')
    approved = models.BooleanField(verbose_name=gettext_lazy("Approved"), default=False)
    intervals_generated = models.BooleanField(verbose_name=gettext_lazy("Intervals generated"), editable=False,
                                              default=False)
    employment = models.ForeignKey(Employment, verbose_name=gettext_lazy("Employment"), null=True,
                                   on_delete=models.CASCADE)

    def days(self):
        # get all matching HolidayDay objects
        holidayDays = HolidayDay.objects.filter(holidayEvent=self).aggregate(sum_days=models.Sum(models.F("weight")))
        # Sum their weight
        # Return it
        weight = holidayDays['sum_days']
        if not weight:
            weight = 0

        return weight

    def first_day(self):
        holidayDays = HolidayDay.objects.filter(holidayEvent=self).aggregate(min_days=models.Min("date"))
        return holidayDays['min_days']

    def last_day(self):
        holidayDays = HolidayDay.objects.filter(holidayEvent=self).aggregate(max_days=models.Max("date"))
        return holidayDays['max_days']

    def __str__(self):
        return self.description

    class Meta:
        verbose_name = gettext_lazy("Holiday Event")
        verbose_name_plural = gettext_lazy("Holiday Events")
        ordering = ["employment__company", "description"]


class HolidayDay(models.Model):
    date = models.DateField(verbose_name=gettext_lazy("Date"), )
    weight = models.DecimalField(verbose_name=gettext_lazy("Weight"), max_digits=2, decimal_places=1, default=1.0)
    description = models.CharField(verbose_name=gettext_lazy("Description"), max_length=200, blank=True, default='')
    holidayEvent = models.ForeignKey(HolidayEvent, verbose_name=gettext_lazy("Holiday Event"), on_delete=models.CASCADE)
    employment = models.ForeignKey(
        Employment,
        verbose_name=gettext_lazy("Employment"),
        null=True,
        on_delete=models.CASCADE)

    def get_start(self):
        return timezone.make_aware(
            datetime.datetime.combine(
                self.date,
                datetime.time(hour=8, minute=0)
            ),
            timezone=timezone.get_current_timezone()
        )

    def get_end(self):
        return timezone.make_aware(
            datetime.datetime.combine(
                self.date,
                datetime.time(hour=20, minute=0)
            ),
            timezone=timezone.get_current_timezone()
        )

    def __str__(self):
        return self.date.strftime('%Y-%m-%d') + ": " + self.description

    class Meta:
        verbose_name = gettext_lazy("Holiday Day")
        verbose_name_plural = gettext_lazy("Holiday Days")
        ordering = ["employment", "date"]
