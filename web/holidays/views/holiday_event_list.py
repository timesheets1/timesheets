from django.http import HttpResponse
from django.shortcuts import get_object_or_404
from django.template import loader

from holidays.models import HolidayEvent
from management.models import Profile, Employment


def holiday_event_list(request, employment_id):
    template = loader.get_template('holiday_event_list.html')
    context = {}
    profile = get_object_or_404(Profile, user=request.user)
    employment = get_object_or_404(Employment, id=employment_id, profile=profile)
    context["employment"] = employment
    holiday_events = HolidayEvent.objects.filter(employment=employment)

    holiday_events = sorted(holiday_events, key=lambda x: x.first_day(), reverse=True)

    context["holiday_events"] = holiday_events
    return HttpResponse(template.render(context, request))
