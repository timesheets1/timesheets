from django.contrib.auth.decorators import login_required
from django.core.exceptions import ValidationError
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import get_object_or_404
from django.template import loader
from django.urls import reverse

from holidays.forms import HolidayEventForm
from holidays.models import HolidayEvent, HolidayDay
from management.models import Profile, Employment


@login_required(login_url='account_login')
def holiday_event_delete(request, employment_id, pk):
    template = loader.get_template('holiday_event_delete.html')
    context = {}
    profile = get_object_or_404(Profile, user=request.user)
    employment = get_object_or_404(Employment, id=employment_id, profile=profile)
    context["employment"] = employment

    holiday_event = get_object_or_404(HolidayEvent, pk=pk, employment=employment)
    context["holiday_event"] = holiday_event
    form = HolidayEventForm(request.POST, instance=holiday_event)

    if request.method == "POST":
        if form.is_valid():  # checks CSRF
            holiday_days = HolidayDay.objects.filter(holidayEvent=holiday_event, employment=employment)
            for day in holiday_days:
                day.delete()
            holiday_event.delete()
            return HttpResponseRedirect(reverse("holiday_event_list", args=[employment_id]))
        else:
            raise ValidationError(form.errors)

    return HttpResponse(template.render(context, request))
