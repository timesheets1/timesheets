from django.contrib.auth.decorators import login_required
from django.http import HttpResponse
from django.shortcuts import get_object_or_404
from icalendar import Calendar, Event

from holidays.models import HolidayEvent, HolidayDay
from management.models import Profile, Employment


@login_required(login_url='account_login')
def holiday_ical_export(request, holiday_event_id):
    profile = get_object_or_404(Profile, user=request.user)
    employments = Employment.objects.filter(
        profile=profile
    )
    holiday_event = get_object_or_404(
        HolidayEvent,
        employment__in=employments,
        id=holiday_event_id,
        approved=True
    )

    holiday_days = HolidayDay.objects.filter(
        holidayEvent=holiday_event,
    )

    cal = Calendar()
    cal.add('prodid', '-//timesheets//mxm.dk//')
    cal.add('version', '2.0')

    i = 1
    for day in holiday_days:
        event = Event()
        event.add('summary', f'{holiday_event.description} {i}/{len(holiday_days)}')
        event.add('dtstart', day.get_start())
        event.add('dtend', day.get_end())
        event.add('dtstamp', day.get_start())
        cal.add_component(event)
        i += 1

    # binary string
    ical_data = cal.to_ical()
    response = HttpResponse(ical_data, content_type="text/calendar")
    response['Content-Disposition'] = "attachment; filename=%s" % f'{holiday_event.description}'
    return response
