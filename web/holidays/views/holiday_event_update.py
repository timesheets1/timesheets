from django.contrib.auth.decorators import login_required
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import get_object_or_404
from django.template import loader
from django.urls import reverse

from holidays.forms import HolidayEventForm, HolidayDayUpdateFormSet
from holidays.models import HolidayEvent
from holidays.utils import handle_approved_holiday_event
from management.models import Profile, Employment


@login_required(login_url='account_login')
def holiday_event_update(request, employment_id, pk):
    template = loader.get_template('holiday_event_update.html')
    context = {}
    profile = get_object_or_404(Profile, user=request.user)
    employment = get_object_or_404(Employment, id=employment_id, profile=profile)
    context["employment"] = employment

    holiday_event = get_object_or_404(HolidayEvent, pk=pk, employment=employment)
    context["holiday_event"] = holiday_event

    if request.method == "POST":
        form = HolidayEventForm(request.POST, instance=holiday_event)
        inline_form_set = HolidayDayUpdateFormSet(request.POST, instance=holiday_event)

        if form.is_valid() and inline_form_set.is_valid():

            event = form.save(commit=True)
            event.employment = employment
            event.save()
            holiday_days = inline_form_set.save(commit=True)
            for day in holiday_days:
                day.employment = employment
                day.save()
            if holiday_event.approved:
                handle_approved_holiday_event(employment, holiday_event)

            return HttpResponseRedirect(reverse("holiday_event_list", args=[employment_id]))
        else:
            context["form"] = form
            context["inline_form_set"] = inline_form_set
            return HttpResponse(template.render(context, request))

    else:
        form = HolidayEventForm(instance=holiday_event)
        inline_form_set = HolidayDayUpdateFormSet(instance=holiday_event)
    context["form"] = form
    context["inline_form_set"] = inline_form_set
    return HttpResponse(template.render(context, request))
