from django.contrib.auth.decorators import login_required
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import get_object_or_404
from django.template import loader
from django.urls import reverse

from holidays.forms import HolodayEventCreateForm
from holidays.models import HolidayEvent, HolidayDay
from management.models import Profile, Employment
from management.utils import contracted_work_time_at

from timesheets.utils import daterange


@login_required(login_url='account_login')
def holiday_event_create(request, employment_id):
    template = loader.get_template('holiday_event_create.html')
    context = {}
    profile = get_object_or_404(Profile, user=request.user)
    employment = get_object_or_404(Employment, id=employment_id, profile=profile)
    context["employment"] = employment

    if request.method == "POST":
        form = HolodayEventCreateForm(request.POST)

        if form.is_valid():
            event = HolidayEvent(
                description=form.cleaned_data.get("description"),
                approved=False,
                intervals_generated=False,
                employment=employment,
            )
            event.save()

            for day in daterange(form.cleaned_data.get("start"), form.cleaned_data.get("end")):
                holiday_day = HolidayDay(
                    date=day,
                    weight=1,
                    holidayEvent=event,
                    employment=employment,
                )
                holiday_day.save()

            return HttpResponseRedirect(reverse("holiday_event_list", args=[employment_id]))
    else:
        form = HolodayEventCreateForm()
    context["form"] = form
    return HttpResponse(template.render(context, request))
