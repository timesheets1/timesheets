from django.contrib.auth.decorators import login_required
from django.db.models import Sum
from django.http import HttpResponse
from django.template import loader
from django.utils import timezone

from holidays.models import HolidayEvent, HolidayDay
from management.models import ContractPeriod
from timesheets.helper import get_employments_for_user
import datetime


@login_required(login_url='/admin/login/')
def dashboard(request):
    template = loader.get_template('holidays_dashboard.html')
    context = {}
    context["user"] = request.user

    employments = get_employments_for_user(request.user)

    if employments:
        data = []
        for employment in employments:

            employment_data = {}
            employment_data["employment"] = employment

            contract_periods = ContractPeriod.objects.filter(employment=employment).order_by('start')

            contracted_holiday_days = 0

            for contract_period in contract_periods:
                start = contract_period.start
                end = contract_period.end
                if not end:
                    end = datetime.date(year=timezone.now().year, month=12, day=31)
                calendar_days = (end - start).days

                holiday_days = (calendar_days * contract_period.holiday_days_amount) / 365

                contracted_holiday_days += holiday_days

            employment_data["contracted_holiday_days"] = contracted_holiday_days

            # reduce contracted holiday days by the amount of days taken to get the available holiday days

            holidays_taken_total: int = 0
            approved_holiday_events = HolidayEvent.objects.filter(employment=employment, approved=True)
            for event in approved_holiday_events:
                holidays_taken_total += event.days()
            holidays_taken_total += employment.offset_holiday_days

            employment_data["holidays_taken_total"] = holidays_taken_total

            available_holiday_days = contracted_holiday_days - holidays_taken_total
            employment_data["available_holiday_days"] = available_holiday_days

            #  calculate holidays taken this year based on HolidayDay objects, since holiday events may span over multiple years
            holidays_taken_this_year = HolidayDay.objects.filter(
                holidayEvent__approved=True,
                employment=employment,
                date__year=datetime.datetime.now().year
            ).aggregate(Sum("weight"))["weight__sum"]
            if not holidays_taken_this_year:
                holidays_taken_this_year = 0
            employment_data["holidays_taken_this_year"] = holidays_taken_this_year

            data.append(employment_data)

        context["employment_data"] = data

        return HttpResponse(template.render(context, request))
    else:
        template = loader.get_template('holiday_no_company.html')
        context = {}
        return HttpResponse(template.render(context, request))
