from django.contrib import admin

from .models import HolidayDay, HolidayEvent


@admin.register(HolidayDay)
class HolidayDayAdmin(admin.ModelAdmin):
    list_display = ['date', 'weight', 'description', 'holidayEvent', 'employment']
    list_filter = ['holidayEvent', 'employment']


class HolidayDayInline(admin.TabularInline):
    model = HolidayDay
    extra = 3


@admin.register(HolidayEvent)
class HolidayEventAdmin(admin.ModelAdmin):
    list_display = ('description', 'days', 'approved', 'intervals_generated', 'employment')
    list_filter = ('approved', 'intervals_generated', 'employment')
    search_fields = ['date', 'description']
    inlines = [HolidayDayInline]
