# Generated by Django 3.0.4 on 2020-09-07 18:16

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('management', '0008_auto_20200906_1814'),
        ('holidays', '0006_auto_20200704_2256'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='holidayday',
            name='user',
        ),
        migrations.RemoveField(
            model_name='holidayevent',
            name='user',
        ),
        migrations.AddField(
            model_name='holidayday',
            name='employment',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='management.Employment'),
        ),
        migrations.AddField(
            model_name='holidayevent',
            name='employment',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='management.Employment'),
        ),
    ]
