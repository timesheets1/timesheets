from django.urls import path

from holidays.views.dashboard import dashboard
from holidays.views.holiday_event_create import holiday_event_create
from holidays.views.holiday_event_delete import holiday_event_delete
from holidays.views.holiday_event_list import holiday_event_list
from holidays.views.holiday_event_update import holiday_event_update
from holidays.views.holiday_ical_export import holiday_ical_export

urlpatterns = [
    path('', dashboard, name='holiday_dashboard'),

    path('event/<employment_id>', holiday_event_list, name='holiday_event_list'),
    path('event/<employment_id>/create', holiday_event_create, name='holiday_event_create'),
    path('event/<employment_id>/update/<pk>', holiday_event_update, name='holiday_event_update'),
    path('event/<employment_id>/delete/<pk>', holiday_event_delete, name='holiday_event_delete'),
    path('event/<int:holiday_event_id>/ical_export', holiday_ical_export, name="holiday_event_ical_export"),
]
