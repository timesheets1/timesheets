from _decimal import Decimal
from datetime import datetime, time, timedelta

from django.utils import timezone as tz

from holidays.models import HolidayDay
from management.utils import contracted_work_time_at
from timesheets.models import Category, Interval


def calculateHolidayAmount(start, end, amount):
    days_in_interval = (end - start).total_seconds() / (60 * 60 * 24) + 1  # count the last day full
    holiday_contribution = Decimal(days_in_interval) / Decimal(365) * amount
    return holiday_contribution


def handle_approved_holiday_event(employment, holiday_event):
    return None

    holiday_days = HolidayDay.objects.filter(holidayEvent=holiday_event)

    ##########
    # fill holiday_category variable with the Category object used for the intervals
    ##########
    holiday_category, created = Category.objects.get_or_create(
        name="Holiday",
        company=holiday_event.employment.company,
        description="Generated for HolidayEvents",
        working_time=True,
        bookable=False,
        exclusive=False,
    )

    for single_day in holiday_days:
        seconds_of_holiday_day = int(
            round(single_day.weight * contracted_work_time_at(employment, single_day.date) * 60 * 60, 0))

        if single_day.holidayInterval:
            # update previously generated holiday Interval

            start = tz.make_aware(datetime.combine(
                date=single_day.date,
                time=time(8, 00),
            ), timezone=tz.get_current_timezone())

            holiday_day_interval = single_day.holidayInterval
            holiday_day_interval.start = start
            holiday_day_interval.end = holiday_day_interval.start + timedelta(seconds=seconds_of_holiday_day)
            holiday_day_interval.category = holiday_category
            holiday_day_interval.company = holiday_event.employment.company
            holiday_day_interval.notes = single_day.holidayEvent.description
            holiday_day_interval.user = single_day.employment.profile.user
            holiday_day_interval.save()
        else:
            # create new Interval instance if none is present before
            start = tz.make_aware(datetime.combine(
                date=single_day.date,
                time=time(8, 00),
            ), timezone=tz.get_current_timezone())

            holiday_day_interval = Interval(
                start=start,
                end=start + timedelta(seconds=seconds_of_holiday_day),
                category=holiday_category,
                company=holiday_event.employment.company,
                notes=single_day.holidayEvent.description,
                user=single_day.employment.profile.user,
            )
            holiday_day_interval.save()
            single_day.holidayInterval = holiday_day_interval
            single_day.save()
