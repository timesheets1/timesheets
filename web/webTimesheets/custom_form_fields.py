from django import forms


class DateInput(forms.DateInput):
	input_type = 'date'

	def format_value(self, value):
		'''
		get rid of i18n-specific formatting,
		since input type date handles that for us
		'''
		return value

# how to set step=1????
# class TimeInput(forms.TimeInput):
#	input_type = 'time'
#