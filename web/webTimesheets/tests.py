from django.conf import settings
from django.test import SimpleTestCase
from django.urls import reverse


class timesheetsDefaultViewTest(SimpleTestCase):

    def test_version_debug_true_no_replace(self):

        with self.settings(
            DEBUG=True,

        ):
            response = self.client.get(reverse("impressum"))
            self.assertEqual(response.status_code, 200)
            self.assertEqual(response.context["version"], "VERSION_STRING")
            self.assertEqual(response.context["commit"], "COMMIT_HASH_STRING")

    def test_version_debug_false_no_replace(self):

        with self.settings(
            DEBUG=False,
        ):
            response = self.client.get(reverse("impressum"))
            self.assertEqual(response.status_code, 200)
            self.assertTrue("version" not in response.context)
            self.assertTrue("commit" not in response.context)

    def test_version_debug_true_replace(self):
        version = "1.2.3"
        commit_hash = "g3w902jlk12r93joi3"

        with self.settings(
            DEBUG=True,
            VERSION=version,
            COMMIT_HASH=commit_hash,
        ):
            response = self.client.get(reverse("impressum"))
            self.assertEqual(response.status_code, 200)
            self.assertEqual(response.context["version"], version)
            self.assertEqual(response.context["commit"], commit_hash)

    def test_version_debug_false_replace(self):
        version = "1.2.3"
        commit_hash = "g3w902jlk12r93joi3"

        with self.settings(
            DEBUG=False,
            VERSION=version,
            COMMIT_HASH=commit_hash,
        ):
            response = self.client.get(reverse("impressum"))
            self.assertEqual(response.status_code, 200)
            self.assertEqual(response.context["version"], version)
            self.assertEqual(response.context["commit"], commit_hash)