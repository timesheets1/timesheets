"""webTimesheets URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.views.generic import RedirectView
from django.urls import include, path

from management.views.impressum import impressum

urlpatterns = [
    path('', include('timesheets.urls')),
    path('i18n/', include('django.conf.urls.i18n')),
    path('prometheus/', include('django_prometheus.urls')),
    path('impressum/', impressum, name='impressum'),
    path('timesheets/', include('timesheets.urls')),
    path('holidays/', include('holidays.urls')),
    path('management/', include('management.urls')),
    path('stats/', include('statistic_module.urls')),
    path('sickness/', include('sickness.urls')),

    path('admin/', admin.site.urls, name="admin"),

    # re_path(r'^accounts/login(?P<path>.*)$', RedirectView.as_view(url='/')),
    path('accounts/', include('allauth.urls')),

    path('invitations/', include('invitations.urls', namespace='invitations')),
]
