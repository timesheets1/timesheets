from django.conf import settings


def version_processor(request):
    data = {}
    version = settings.VERSION
    if settings.DEBUG or version != "VERSION_STRING":
        data["version"] = version

    commit = settings.COMMIT_HASH
    if settings.DEBUG or commit != "COMMIT_HASH_STRING":
        data["commit"] = commit
    return data


def plausible_snippet_version_processor(request):
    data = {}
    snippet = settings.PLAUSIBLE_SNIPPET
    if snippet:
        data["plausible_snippet"] = snippet

    return data


def currency_processor(request):
    data = {}
    if settings.CURRENCY:
        data["currency"] = settings.CURRENCY

    return data