// This is a minimal config.
// If you need the full config, get it from here:
// https://raw.githubusercontent.com/tailwindlabs/tailwindcss/v1/stubs/defaultConfig.stub.js

const defaultTheme = require('tailwindcss/defaultTheme')

module.exports = {
  content: [
      // Templates within theme app (e.g. base.html)
      './templates/**/*.html',
      // Templates in other apps
      './**/templates/**/*.html',
    ],
  theme: {
    extend: {
       colors:{
        primarycolor: {
          50: '#f0f6db',
          100: '#e0edb7',
          200: '#cfe493',
          300: '#bddb6e',
          400: '#aad246',
          500: '#95c900',
          600: '#7ba410',
          700: '#628115',
          800: '#4a5f15',
          900: '#334013',
        },
      },
    },
  },
  plugins: [],
}