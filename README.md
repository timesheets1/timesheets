# Timesheets in Django!

## Deployment

### Docker Images
This project provides docker images for easy deployment. It is easiest to use them with the provided docker-compose.yml

`web` contains the django application. It can be configured with a range of environment variables. It also requires the presence of a postgres database.

`proxy` this is a nginx reverse proxy configured to serve the static files from django. The static files need to be provided via a volume.

### From source

Basically follow the instructions for django: [https://docs.djangoproject.com/en/3.1/howto/deployment/](https://docs.djangoproject.com/en/3.1/howto/deployment/)

This project uses tailwindcss, so you will need to build the css before deployment, see below.

## Update 

### Docker
Update your docker-compose.yml to use a newer version tag.

``` 
docker-compose pull
docker-compose up -d
```

### From source
```
git pull
```
Then, restart your application.


## Development

### Database

The folder dev-db contains docker compose file that defines a postgres database that you can develop against.

```bash
cd dev-db
docker-compose up -d
cd ..
```

### Render CSS

Open a second shell and leave the last command running
```bash
cd web
make install
make build 
```

### Django Migrations
The database is managed by the django orm. See the makefile for details.
```
make migrate
```

### Django application
In order to start the django application:
```bash
python3 -m venv venv
source venv/bin/activate
cd web
make migrate
make
```

#### Create a Superuser
If the database is empty, you probably want to create a superuser:
```bash
python3 manage.py createsuperuser
```
Allauth requires verified email addresses on login.
This can be circumvented by logging into the admin interface at `/admin/`
and adding a verified, primary email address manually.

#### Render Translations

To render the translations, use:
```bash
make compilemessages
```
See makefile for details. This needs Gnu gettext tools to be installed.

## Release Branches

This project uses release branches in the format of v.1.1.1-text.

The pipeline jobs that generate images use a regex in order to see if they are running on a release branch or a feature branch.
Hence, great care has to be given to branch naming.

## Environment Variables

You can use these environment variables:
```shell
export DJANGO_DEBUG=True

export EMAIL_HOST=host.mail.com
export EMAIL_HOST_PASSWORD="password"
export EMAIL_HOST_USER=admin@timesheets.de
export EMAIL_PORT=587
export EMAIL_USE_TLS="True"
export DEFAULT_FROM_EMAIL="admin@timesheets.de"
export INVITATIONS_ONLY="False"
```

Add your email information and save these lines in `setEnvironment.yml`.
